// import { Gender, IPhoneInput, StatusCode } from './Common';

export enum Role {
  Musician = 'Musician',
  Fan = 'Fan',
  Business = 'Business',
  Empty = ''
}
export enum Profession {
  Band = 'band',
  DJ = 'dj',
  Soloist = 'soloist',
  Acappella = 'acappella',
  Orchestra = 'orchestra',
  Choir = 'choir',
  Musician = 'musician',
  A_R_Administrator = 'A&RAdministrator',
  A_R_Coordinator = 'A&RCoordinator',
  Dance = 'dance',
  EasyListening = 'easyListening',
  Electronic = 'electronic',
  Enka = 'enka',
  FrenchPop = 'frenchPop'

}
export interface IUserSignupForm {
  username: string;
  email: string;
  password: string;
  passwordconfirm: string;
  role: Role;
  country: string;
  isUpdate?: boolean;
  provider?: string;
}

export enum signUpProvider {
  facebook = 'facebook',
  facebookFirebase = 'facebook.com'
}

export const userSignupFormDefault = (): IUserSignupForm => {
  return {
    username: '',
    email: '',
    password: '',
    passwordconfirm: '',
    role: Role.Empty,
    country: '',
    provider: ''
  };
};

export interface IUserForm {
  firstName?: string;
  lastName?: string;
  // gender?: Gender;
  username: string;
  displayName?: string;
  bio?: string;
  birthday?: Date;
  country: string;
  role: Role;
  profession?: Profession[];
  email: string;
  webPage?: string;
  countryCode?: string;
  // phone?: IPhoneInput;
  password?: string;
}

export interface LanguageType {
  title: string;
  code: string;
}

export interface IUser extends IUserForm {
  id: string;
  avatar?: string;
  cover?: string;
  permissions?: string[];
  isBlock?: boolean; // other user is blocking by auth
  isBlocked?: boolean; // auth is blocked by the user
  isVerified?: boolean;
  uLanguage?: LanguageType;
}

export interface IUserFilter {
  keyword?: string;
  // status?: StatusCode;
}

export interface ISimpleUser {
  id: string;
  firstName: string;
  lastName: string;
  email?: string;
  displayName: string;
  avatar: string;
  username?: string;
  isFollowing?: boolean;
  role?: string;
  isVerified?: boolean;
}

export interface IProfile extends IUser {
  numPosts?: number;
  numFollowers?: number;
  balance?: number;
  numFollowing?: number;
  emailVerified?: boolean;
}

export const profileDefault = (): IProfile => {
  return {
    id: '',
    avatar: '',
    balance: 0,
    // gender: Gender.Empty,
    country: '',
    email: '',
    numPosts: 0,
    numFollowers: 0,
    role: Role.Empty,
    username: '',
    // phone: {
    //   phoneCode: null,
    //   phoneNumber: null
    // },
    profession: []
  };
};

export enum FavoriteType {
  Message = 'message',
  Plan = 'plan',
  Project = 'project',
  ProjectTask = 'project-task',
  PlanItem = 'plan-item'
}
export interface IFavoriteForm {
  type: FavoriteType;
  relateId: string;
}

export interface IFavorite extends IFavoriteForm {
  id: string;
  title: string;
  description: string;
  avatar: string;
  metadata?: any;
}

export interface IChangePasswordForm {
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
}

export const changePasswordFormDefault = (): IChangePasswordForm => {
  return {
    currentPassword: '',
    newPassword: '',
    confirmPassword: ''
  };
};

export interface FormCreateAccount { 
  firstname: string;
	lastName: string;
	email: string;
	password: string;
	birthday: Date;
	phone: string;
	avatar: string;
	schoolName: string;
	roleId: string;
}

