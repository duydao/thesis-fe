export enum RouterDictionaryName {
  SearchPost = 'searchPost',
  SearchHashtag = 'searchHashtag',
  FeedDetail = 'feedDetail',
  FeedList = 'feed-list',
  SignUp = 'Signup',
  ForgotPassword = 'ForgotPassword',
  ResetPassword = 'ResetPassword',
  OtherProfile = 'OtherProfile',
  MyProfile = 'myProfile',
  EditProfile = 'EditProfile',
  CompetitionListHome = 'CompetitionListHome',
  ShareFeed = 'ShareFeed',
  ShareProfile = 'ShareProfile'
}

export interface IRouterMeta {
  isGuest?: boolean;
  keepPosition?: boolean;
  isPrivate?: boolean;
}
