export interface Subject {
  name: string;
  courses: Course[];
}

export interface Course {
  title: string;
  time: number;
  description: string;
  dayLearning: DayLearning[];
}

export interface DayLearning {
  name: string;
  description: string; 
  challenge: Challenge;
  children: Lession[];
}

export interface Challenge {
  status: boolean;
  typeCh: TypeChallenge;
  topic: string;
  answer: string;
  score: number | null;
  subIndex: SubIndex[];
}

export interface SubIndex {
  name: string;
  score: number | null;
}

export interface Lession {
  name: string;
  description: string;
  children: Question[];
}

export interface Question {
  name: string;
  type: TypeQuestion;
  score: number | null;
  answer: Answer[];
}

export interface Answer {
  answer: string;
  isCorrect: boolean;
}

export enum TypeQuestion {
  'ESSAY',
  'KNOWLEDGE',
  'MULTIPLE',
}

export enum TypeChallenge {
  'PROJECT',
  'PRESENTATION',
}

// export enum Score {
//   '1 point',
//   '2 points',
//   '3 points',
//   '4 points',
//   '5 points',
// }
