export interface Report {
  day_report: String,
  your_report: String,
  reply: Reply[],
}

export interface Reply {
  time_reply: String,
  author: String,
  reply: String,
}
