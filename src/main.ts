import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store, { ActionType } from './store'
import Antd  from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import './assets/css/custom.scss';
import { sync } from 'vuex-router-sync';

Vue.use(Antd);
sync(store, router)
// require('dotenv').config()

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    const user = JSON.parse(localStorage.getItem('@user') || '{}')
    this.$store.dispatch(ActionType.ChangeStateAuth, { router: this.$router, user: user })
  }
}).$mount('#app')
