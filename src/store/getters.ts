import { IAvatarDefault, IState } from './types';

export const getters = {
  avatarDefault(): IAvatarDefault {
    return {
      user: '/assets/img/web-avatar-72-80.svg',
      image: '/assets/img/image-placeholder.svg',
      video: '/assets/img/video-placeholder.svg',
      project: '/assets/img/project-placeholder.png',
      plan: '/assets/img/plan-favourite.svg',
      message: '/assets/img/message-favourite.svg'
    };
  },
  getAuthProfile(state: IState) {
    return () => {
      return state.global.authUser;
    };
  }
};
