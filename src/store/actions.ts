import VueRouter from 'vue-router';
import { ActionContext } from 'vuex';
import { ActionType, IState, MutationType } from './types';
import { sfrHttpClient } from '../api';
import { loginParams } from 'models/Authentication';
import { isNull } from 'lodash';

const { authenticate } = sfrHttpClient.auth;
const { getCoursesList } = sfrHttpClient.course;

export const actions = {
  async [ActionType.Authenticated](
    { dispatch, commit }: ActionContext<IState, IState>, payload: loginParams
  ) {
    try {
      const user = await authenticate(payload);
      if(user && user.id) {
        commit(MutationType.SetProfile, {user: user})
        localStorage.setItem('@user', JSON.stringify(user))
        window.location.href = '/dashboard'
      } else {
        dispatch(ActionType.CatchException, 'login fail');
      }

    } catch (error) {
      dispatch(ActionType.CatchException, error);
    }
  },

  async [ActionType.ChangeStateAuth](
    { commit, dispatch, state }: ActionContext<IState, IState>,
    { router, user }: any
  ) {
    if (user && user.id) {
      commit(MutationType.SetProfile, {user: user})
    }
    if (state.global.authUser) {
      if(state.route.path === '/login') {
        router.push('/dashboard')
      }
    }else {
      if(state.route.path != '/login' && state.route.meta?.isPrivate) {
        router.push('/login')
      }
    }

  },
  
  [ActionType.CloseTopAlert](
    { commit }: ActionContext<IState, IState>
  ) {
    commit(MutationType.CloseTopAlert);
  },

  async [ActionType.GetDataCourses](
    { dispatch, commit }: ActionContext<IState, IState>
  ) {
    try {
      const courses = await getCoursesList();
    } catch (error) {
      dispatch(ActionType.CatchException, error);
    }
  },

  async [ActionType.GetDataCourses](
    { dispatch, commit }: ActionContext<IState, IState>
  ) {
    try {
      const courses = await getCoursesList();
    } catch (error) {
      dispatch(ActionType.CatchException, error);
    }
  },
};
