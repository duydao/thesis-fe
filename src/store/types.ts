
// ---------------------- import all states in here -------------------
import { IProfile, IRouterMeta, LanguageType } from 'models';
import { IUserState } from 'containers/account/Store';
// tslint:disable max-file-line-count

// define interface
export interface IGlobalState {
  drawer: boolean;
  isLoading: boolean;
  lastRoutePath: string;
  currentLocale: string;
  authUser: IProfile;
  topAlert: IAlertState;
  signInErrorMessage: string;
  layout: Layout;
  globalConfig: IGlobalConfiguration;
  menuIsCollapse: boolean;
  isRegisting: boolean;
  isLoging: boolean;
  languages: LanguageType[];
  maintenance: MaintenanceType;
}

export enum TypeAlert {
  Success = 'success',
  Warning = 'warning',
  Info = 'info',
  Error = 'error'
}

export interface IAlertState {
  opened: boolean;
  message: string;
  type: TypeAlert;
  icon: string;
}

export interface MaintenanceType {
  maintenance: boolean;
  imageUrl: string;
  title: string;
  description: string;
}

export interface IRouteState {
  path: string;
  params: any;
  query: any;
  meta?: IRouterMeta;
}

export interface IPaginationState {
  page?: number;
  size?: number;
  nextPageToken?: number;
  total?: number;
}

export interface IState {
  global: IGlobalState;
  route: IRouteState;
  user: IUserState;
}

export interface IFormState<T> {
  payload: T;
  valid: boolean;
  errorMessage: string;
}

export interface IBreadcrumbItem {
  text: string;
  link?: string;
}

export interface IGlobalConfiguration {
  systemMessage: string;
  systemStatus: number;
  role: string[];
  countries: string[];
  gender: string[];
  profession: string[];
}

export enum Layout {
  Home = 'home-layout',
  Login = 'login-layout'
}

export interface IAvatarDefault {
  user: string;
  image: string;
  video: string;
  project: string;
  plan: string;
  message: string;
}

export interface ICustomLazy {
  src: string;
  loading?: string;
  error?: string;
}

// function
export function PaginationState(params?: IPaginationState): IPaginationState {
  return {
    page: 1,
    size: 20,
    ...params,
  };
}

// define type screen

export enum ViewPortSize {
  Mobile = 768,
  Tablet = 1023,
  Desktop = 1215,
  Widescreen = 1407,
}

// define type mutation and action

export enum MutationType {
  // global
  SetCurrentLocale = 'setCurrentLocale',
  ClosePlashScreen = 'closePlashScreen',
  WindowResized = 'windowResized',
  OpenPlashScreen = 'openPlashScreen',
  SetLayout = 'setLayout',
  SetGlobalConfig = 'setGlobalConfig',
  SetMenuCollapse = 'setMenuCollapse',
  OpenTopAlert = 'openTopAlert',
  CloseTopAlert = 'closeTopAlert',
  SetLanguages = 'setLanguages',
  SetMaintenance = 'setMaintenance',
  // users
  SetListUsers = 'setListUsers',
  CreateUser = 'createUser',
  UpdateUser = 'updateUser',
  // login
  Authenticated = 'authenticated',
  Unauthenticated = 'unauthenticated',
  SetProfile = 'setProfile',
  UpdateProfile = 'updateProfile',

  // profile
  LoadingProfile = 'loadingProfile',
  ClearStore = 'clearStore',
  SetRegisting = 'SetRegisting',
  SetRegisted = 'SetRegisted',
  SetLoging = 'SetLoging',
  SetLogon = 'SetLogon',

  SetFirstLogon = 'SetFirstLogon',
  SetNewNotification = 'SetNewNotification'
}

export enum ActionType {
  // global
  InitializeSettings = 'initializeSettings',
  CatchException = 'catchException',
  SetLayout = 'setLayout',
  SetGlobalConfig = 'setGlobalConfig',
  GoBack = 'goBack',
  GoToRoute = 'goToRoute',
  CollapseMenu = 'collapseMenu',
  OpenTopAlert = 'openTopAlert',
  CloseTopAlert = 'closeTopAlert',

  // navigator
  DirectNavigator = 'directNavigator',

  // login
  ChangeStateAuth = 'changeStateAuth',
  Authenticated = 'authenticated',
  Unauthenticated = 'unauthenticated',
  SendEmailVertify = 'SendEmailVertify',
  
  // profile
  UpdateProfile = 'updateProfile',
  LoadingProfile = 'loadingProfile',
  ReloadProfile = 'reloadProfile',

  // user-notification-setting
  UserNotificationSettingUpdate = 'userNotificationSettingUpdate',

  GetDataCourses = 'getDataCourses',
  
}
