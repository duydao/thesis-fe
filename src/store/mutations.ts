import { assign, cloneDeep, omit } from 'lodash';
import {
  IAlertState, IGlobalConfiguration,
  IState, Layout, MutationType, TypeAlert
} from './types';

export function AlertState(): IAlertState {
  return {
    opened: false,
    type: TypeAlert.Info,
    message: 'Text message',
    icon: 'info'
  };
}

export const globalState = {
  global: {
    isLoading: true,
    drawer: true,
    isRegisting: false,
    isLoging: false,
    authUser: null,
    topAlert: AlertState(),
    layout: Layout.Home,
    globalConfig: {
      countries: [],
      role: [],
      gender: [],
      profession: []
    },
    menuIsCollapse: false,
    languages: [],
    maintenance: {
      maintenance: false,
      imageUrl: '',
      title: '',
      description: ''
    }
  }
};

export const mutations = {
  [MutationType.OpenTopAlert](state: IState, topAlert: IAlertState) {
    state.global.topAlert = {
      ...topAlert,
      opened: true
    };
  },
  [MutationType.SetProfile](state: IState, payload: any) {
    state.global.authUser = payload.user
  },
  [MutationType.CloseTopAlert](state: IState) {
    state.global.topAlert = AlertState();
  },
  // login
};
