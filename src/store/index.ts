import Vue from 'vue';
import Vuex from 'vuex';
import { actions } from './actions';
import { getters } from './getters';
import { globalState, mutations } from './mutations';

// module
import * as user from '../containers/account/Store';


export * from './types';

Vue.use(Vuex);

export default new Vuex.Store({
  actions,
  getters,
  mutations,
  state: <any> globalState,
  modules: {
    user
  }
});