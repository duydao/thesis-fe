import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { sfrHttpClient } from './../../../api';

@Component({
  template: require('./view.html'),
  props: {
    text: String,
  },
})

export class ManageSubIndex extends Vue {
  mounted() {
    this.hardData();
  }

  dataList: any = []
  editingKey = ''
  cacheData: any
  total = 0
  columns = [
    {
      title: 'name',
      dataIndex: 'name',
      width: '25%',
      scopedSlots: { customRender: 'name' },
    },
    {
      title: 'description',
      dataIndex: 'description',
      width: '40%',
      scopedSlots: { customRender: 'description' },
    },
    {
      title: 'rate',
      dataIndex: 'rate',
      width: '15%',
      scopedSlots: { customRender: 'rate' },
    },
    {
      title: 'operation',
      dataIndex: 'operation',
      scopedSlots: { customRender: 'operation' },
    },
  ];

  async hardData() {
    this.dataList = await sfrHttpClient.subIndex.getList().catch(() => []);
  }

  handleChange(value: any, id: any, column: any) {
    const newData = [...this.dataList];
    const target = newData.filter(item => id === item.id)[0];
    if (target) {
      target[column] = value;
      this.dataList = newData;
    }
  }

  async handleAdd() {
    await sfrHttpClient.subIndex.createItem({
      description: "Please edit this description",
      rate: 0,
      name: 'New index',
    })
    .then(r => {
      this.dataList = [r, ...this.dataList];
    })
    .catch((e) => {
      this.$message.error(e.message || "Error");
    })
  }

  edit(id: any) {
    const newData = [...this.dataList];
    const target = newData.filter(item => id === item.id)[0];
    this.editingKey = id;
    if (target) {
      target.editable = true;
      this.dataList = newData;
    }
  }

  async save(id: any) {
    const newData = [...this.dataList];
    const newCacheData = this.cacheData ? [...this.cacheData] : [];
    const target = newData.find(item => id === item.id);
    
    
    if (this.checkTotal(newData) && target) {
      
      await sfrHttpClient.subIndex.updateItem({
        description: target.description,
        rate: target.rate,
        name: target.name,
      }, id)
      .then(r => {
        delete target.editable;
        this.dataList = this.dataList.map((it: any) => {
          if(it.id === id){
            it = r;
          }
          return it;
        })
        this.cancel(id)
      })
      .catch((e) => {
        this.$message.error(e.message || "Error");
      })
    }
    this.editingKey = '';
  }

  checkTotal(data: any) {
    const targetElement = document.getElementById('warning-rate') as HTMLDivElement
    this.total = 0
    for(let i = 0; i < data.length; i++) {
      data[i].rate = parseInt(data[i].rate)
      this.total = this.total + data[i].rate
    }
    if (this.total > 100) {
      this.$notification['error']({
        message: 'Total rate are bigger than 100%',
        description: `Total rate must be equal to 100%. Now is ${this.total}%`,
        duration: 10,
      });
      this.hidden(targetElement)
      return false;
    }
    else if (this.total < 100) this.visible(targetElement)
    else {
      this.hidden(targetElement)
      this.$notification['success']({
        message: 'Update Sucessfull',
        description: '',
      });
    }
    return true
  }

  visible(element: any) {
    element.style.visibility = "visible"
    element.style.opacity = "1"
    element.style.transitionDelay = "0s"
    element.style.margin = "30px"
  }

  hidden(element: any) {
    element.style.visibility = "hidden"
    element.style.opacity = "0"
    element.style.transition = "visibility 0s linear 0.33s, opacity 0.33s linear"
    element.style.margin = "0px"
  }

  cancel(id: any) {
    const newData = [...this.dataList];
    const target = newData.find(item => id === item.id);
    console.log(target)
    this.editingKey = '';
    if (target) {
      Object.assign(target, this.cacheData?.find((item: any) => id === item.id) || {});
      delete target.editable;
      this.dataList = newData;
    }
    if(!this.checkTotal(this.dataList)){
      this.hardData();
    }
  }

  async onDelete(id: string) {
    await sfrHttpClient.subIndex.deleteItem(id)
    .then(r => {
      const dataSource = [...this.dataList];
      this.dataList = dataSource.filter(item => item.id !== id);
    })
    .catch((e) => {
      this.$message.error(e.message || "Error");
    })
  }
}