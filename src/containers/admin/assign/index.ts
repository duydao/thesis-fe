import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { sfrHttpClient } from './../../../api';

@Component({
  template: require('./view.html'),
})

export class Assign extends Vue {
  public listMentors = []
  public listMentees = []
  public listSubjects = []
  public listCourses = []

  public form = {
    mentorId: null,
    internId: null,
    subjectId: null,
    coursesId: null,
  }

  mounted() {
    this.getListDataDefault();
  }

  async clickSubject(subject: any){
    this.form.coursesId = null;
    this.listCourses = await sfrHttpClient.subjects.getCoursesBySubject(subject.id).catch(() => []);
  }

  async handleSubmit(){
    const {  form } = this;
    if(form.mentorId && form.internId && form.subjectId && form.coursesId){
      sfrHttpClient.assigns.createAssigns(form)
        .then(() => {
          this.form = {
            mentorId: null,
            internId: null,
            subjectId: null,
            coursesId: null,
          }
          this.$message.success('Assign success')
        })
        .catch(e => {
          this.$message.error(e.message)
        })
    }else this.$message.error('Form invalid')
  }

  private async getListDataDefault(){
    this.listSubjects = await sfrHttpClient.subjects.listSubject().catch(() => []);
    const roles = await sfrHttpClient.roles.listRoles().catch(() => null);
    if(roles){
      const roleMenter = roles.find((it: any) => it.name === 'Mentor');
      this.listMentors = await sfrHttpClient.users.getListUser(roleMenter.id).catch(() => []);

      const roleInter = roles.find((it: any) => it.name === 'Intern');
      this.listMentees = await sfrHttpClient.users.getListUser(roleInter.id).catch(() => []);

    }
    
  }
}
