import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { ActionType } from '../../../store';
import { sfrHttpClient } from './../../../api';
 
@Component({
  template: require('./view.html'),
  name: "edit-account",
})

export class EditAccount extends Vue {
  data() {
    return {
      dateFormatList: ['DD/MM/YYYY','D/M/YYYY', 'DD/M/YYYY', 'D/MM/YYYY'],
    };
  }

  visible = false
  fileList = []
  avatar = '';
  public form: any
  public user: any = 'abc'
  
  mounted() {
    this.getUser()
  }

  async getUser() {
    this.user = await sfrHttpClient.user.getCurrentUser()
    console.log(this.user)
  }

  beforeCreate() {
    this.form = this.$form.createForm(this, { name: 'register' });
  }
  showModal() {
    this.visible = true;
  }
  async handleOk() {
    this.visible = false;
    if(this.avatar){
      const form = new FormData();
      form.append('attachment', this.avatar);

      this.user.avatar = await sfrHttpClient.attachment.upload(form)
        .then(res => {
         return res.filePath;
        })
        .catch(e => {
          this.$message.error(e.message);
          return null;
        })
      if(!this.user.avatar) return;
    }
    window.location.reload()
    await sfrHttpClient.users.updateUser(this.user)
    
  }

  previewFiles(event: any) {
    this.avatar = event.target.files[0] || null;
  }
}

interface FormDataType {
  email: string;
  firstName: string;
  lastName: string;
  birthday: Date;
  phone: number;
  avatar: string;
  school: string;
  role: Role;
}

enum Role {
  admin,
  mentor,
  mentee,
}
