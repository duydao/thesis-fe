import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { sfrHttpClient } from './../../../api';
import moment from 'moment';
@Component({
  template: require('./view.html'),
})

export class NewAccount extends Vue {
  mounted() {
    this.getListRoles();
  }

  data() {
    return {
      dateFormatList: ['DD/MM/YYYY','D/M/YYYY', 'DD/M/YYYY', 'D/MM/YYYY'],
      // previewVisible: false,
      // previewImage: '',
      // fileList: []
    };
  }
  roles = [];
  previewVisible = false
  previewImage = ''
  fileList = []
  public moment: any
  public form: any
  public confirmDirty: any
  public FormDataType: FormDataType = {
    email: '',
    firstName: '',
    lastName: '',
    birthday: new Date,
    phone: 123,
    avatar: '',
    school: '',
    role: 2,
  }
  isLoading = false;
  timeNow = moment(new Date(), 'DD/MM/YYYY');
  avatar = '';

  beforeCreate() {
    this.form = this.$form.createForm(this, { name: 'register' });
  }

  async handleSubmit(e: any) {
    e.preventDefault();
    this.form.validateFieldsAndScroll( async (err: any, values: any) => {
      if (!!err) {
        console.log('Received values of form: ', err);
        return;
      }

      const data = {
        firstname: values['first-name'],
        lastName: values['last-name'],
        email: values.email,
        password: values.password,
        birthday: moment(values.birthday).toDate(),
        phone: values.phone,
        avatar: "https://www.denofgeek.com/wp-content/uploads/2016/11/avatar-sequel.jpg",
        schoolName: values.schoolName,
        roleId: values.roleId
      }
      this.isLoading = true;
      if(this.avatar){
        const form = new FormData();
        form.append('attachment', this.avatar);

        data.avatar = await sfrHttpClient.attachment.upload(form)
          .then(res => {
           return res.filePath;
          })
          .catch(e => {
            this.$message.error(e.message);
            this.isLoading = false;
            return null;
          })
        if(!data.avatar) return;
      }
      
      sfrHttpClient.users.createAccount(data)
        .then(res => {
          this.$message.success('Create success');
        })
        .catch(e => {
          this.$message.error(e.message);
        })
        .finally(() => this.isLoading = false)
      
      
    });
  }
  handleConfirmBlur(e: any) {
    const value = e.target.value;
    this.confirmDirty = this.confirmDirty || !!value;
  }
  compareToFirstPassword(rule: any, value: any, callback: any) {
    const form = this.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  }
  validateToNextPassword(rule: any, value: any, callback: any) {
    const form = this.form;
    if (value && this.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }
  getBase64(file: any) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }
  handleCancel() {
    this.previewVisible = false;
  }
  async handlePreview(file: any) {
    if (!file.url && !file.preview) {
      file.preview = await this.getBase64(file.originFileObj);
    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
  }
  handleChange({ fileList }: {fileList: any}) {
    this.fileList = fileList;
  }
  previewFiles(event: any) {
    this.avatar = event.target.files[0] || null;
  }

  private async getListRoles(){
    const roles = await sfrHttpClient.roles.listRoles().catch(() => []);
    this.roles = roles;
  }
}

interface FormDataType {
  email: string;
  firstName: string;
  lastName: string;
  birthday: Date;
  phone: number;
  avatar: string;
  school: string;
  role: Role;
}

enum Role {
  admin,
  mentor,
  mentee,
}
