// import { cloneDeep, concat, findIndex, map } from 'lodash';
import { profileDefault, IUser } from '../../../models';
import { MutationTypeUser } from './types';

export interface IUserState {
  loading: boolean;
  loadingMore: boolean;
  userDetailLoading: boolean;
  detail: IUser;
  accountList: any;
}

export const defaultState: IUserState = {
  loading: false,
  loadingMore: false,
  userDetailLoading: false,
  detail: profileDefault(),
  accountList: []
};

export const mutations = {
  [MutationTypeUser.SetUserLoadingMore](state: IUserState) {
    state.loadingMore = true;
  },
  [MutationTypeUser.SetUserLoadedMore](state: IUserState) {
    state.loadingMore = false;
  },
  [MutationTypeUser.SetUsersList](state: IUserState, payload: any) {
    state.accountList = payload.data;
  },
};
