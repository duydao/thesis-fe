export enum ActionTypeUser {
  GetUsersList = 'GetUsersList',
  GetUsersList1 = 'GetUsersList1',
  GetDetailUser = 'getDetailUser'
}

export enum MutationTypeUser {
  SetUsersList = 'SetUsersList',
  UpdateUsersList = 'UpdateUsersList',
  ResetUsersList = 'ResetUsersList',
  SetUserLoading = 'userLoading',
  SetUserLoaded = 'userLoaded',
  SetUserLoadingMore = 'SetUserLoadingMore',
  SetUserLoadedMore = 'SetUserLoadedMore',
  SetDetailUserLoaded = 'SetDetailUserLoaded',
  SetDetailUserLoading = 'SetDetailUserLoading',
  SetDetailUser = 'SetDetailUser',
  ResetDetailUser = 'resetDetailUser'
}
