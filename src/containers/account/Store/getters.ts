import { IUserState } from './mutations';

export const getters = {
  getCurrentProfileId(state: IUserState) {
    return () => {
      return state.detail.id;
    };
  }
};
