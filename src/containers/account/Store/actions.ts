import { stripObject } from '@hgiasac/helper';
import { omit } from 'lodash';
import { sfrHttpClient } from '../../../api';
import { ActionType, IState } from '../../../store';
import { ActionContext } from 'vuex';
import { IUserState } from './mutations';
import { ActionTypeUser, MutationTypeUser } from './types';

const { getUsersList } = sfrHttpClient.user;

export const actions = {
  async [ActionTypeUser.GetUsersList](
    {commit, dispatch}: ActionContext<IUserState, IState>, dataFilter: any
  ) {
    try {
      const data = await getUsersList({});
      commit(MutationTypeUser.SetUsersList, data);
    } catch (error) {
      dispatch(ActionType.CatchException, error);
    }
  },
};
