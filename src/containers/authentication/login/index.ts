import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { emailValidate } from 'src/helpers/validate-form'
import { ActionType } from 'src/store/types'
@Component({
  template: require('./view.html'),
})

export class Login extends Vue {

  public form = {
    email: '',
    password: ''
  }

  mounted() {
    this.handelInput();
  }

  checkForm = (e: Event) => {
    e.preventDefault()
        
    if(emailValidate(this.form.email)  && this.form.password) {      
      this.$store.dispatch(ActionType.Authenticated, this.form)
    }
  }

  handelInput(){
    const inputs = document.querySelectorAll(".input");
    inputs.forEach(input => {
      input.addEventListener("focus", function(this: any){
        const parent = this.parentNode.parentNode;
        parent.classList.add("focus");
      });

      input.addEventListener("blur", function(this: any){
        const parent = this.parentNode.parentNode;
        if(this.value == ""){
          parent.classList.remove("focus");
        }
      });
    })
  }
}