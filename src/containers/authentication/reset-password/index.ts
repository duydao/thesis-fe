import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { sfrHttpClient } from '../../../api';
// import { addScrtip } from 'helpers';
@Component({
  template: require('./view.html'),
})

export class ResetPassword extends Vue {

  public form = {
    password: '',
    token: ''
  }

  loading = false;

  mounted() {
    this.addFont();
    this.handelInput();
    if(!!this.$route?.query?.token){
      this.form.token = this.$route.query.token.toString();
    }else{
      this.$router.push('/login')
    }
  }

  handelInput(){
    const inputs = document.querySelectorAll(".input");
    inputs.forEach(input => {
      input.addEventListener("focus", function(this: any){
        const parent = this.parentNode.parentNode;
        parent.classList.add("focus");
      });

      input.addEventListener("blur", function(this: any){
        const parent = this.parentNode.parentNode;
        if(this.value == ""){
          parent.classList.remove("focus");
        }
      });
    })
  }

  addFont(){
    // addScrtip("https://kit.fontawesome.com/a81368914c.js");
  }

  async handleSubmit(){
    if(this.form.password) {
      this.loading = true;    
      await sfrHttpClient.auth.changePassword(this.form)
        .then(() => {
          this.$message.success('Change password success!');
          setTimeout(() => {
            this.$router.push('/login')
          }, 1000)
        })
        .catch((e) => {
          this.$message.error(e?.message || 'Error')
        })
        .finally(() => this.loading = false)
    }else{
      this.$message.error('Password invalid')
    }
  }
}