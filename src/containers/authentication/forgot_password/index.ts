import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { sfrHttpClient } from '../../../api';
import { emailValidate } from 'src/helpers/validate-form'
import VueRouter from 'vue-router';
// import { addScrtip } from 'helpers';
@Component({
  template: require('./view.html'),
})

export class ForgotPassword extends Vue {
  
  public form = {
    email: ''
  }

  loading = false;

  mounted() {
    this.addFont();
    this.handelInput();
    
  }

  handelInput(){
    const inputs = document.querySelectorAll(".input");
    inputs.forEach(input => {
      input.addEventListener("focus", function(this: any){
        const parent = this.parentNode.parentNode;
        parent.classList.add("focus");
      });

      input.addEventListener("blur", function(this: any){
        const parent = this.parentNode.parentNode;
        if(this.value == ""){
          parent.classList.remove("focus");
        }
      });
    })
  }

  addFont(){
    // addScrtip("https://kit.fontawesome.com/a81368914c.js");
  }

  handleSubmit(){
    if(emailValidate(this.form.email)) {
      this.loading = true;    
      sfrHttpClient.auth.forgotPassword(this.form)
        .then(() => {
          this.$message.success('Submit success, Please check mail');
          setTimeout(() => {
            this.$router.push('/login')
          }, 1000)
        })
        .catch((e) => {
          this.$message.error(e?.message || 'Error')
        })
        .finally(() => this.loading = false)
    }else{
      this.$message.error('Email invalid')
    }
  }
}