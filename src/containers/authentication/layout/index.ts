import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { EditAccount } from '../../account/edit_account';
import { sfrHttpClient } from './../../../api';

@Component({
  template: require('./view.html'),
  components: { EditAccount }
})

export class Layout extends Vue {
  collapsed =  false;
  visibleDrawer = false;
  user: any = {};
  public defaultKey = ['0']

  mounted() {
    this.getUser()
  }
  
  async getUser() {
    this.user = await sfrHttpClient.user.getCurrentUser()
    console.log(this.user)
    this.getCurrentSite()
  }

  getCurrentSite() {
    const route_name = this.$route.name

    switch (route_name) {
      case 'dashboard':
        this.defaultKey = ['1']
        break;
      case 'courses':
        this.defaultKey = ['2']
        break;
      case 'course':
        this.defaultKey = ['3']
        break;
      case 'manage-courses':
        this.defaultKey = ['5']
        break;
      case 'new-course':
        this.defaultKey = ['6']
        break;
      case 'reports':
        this.defaultKey = ['8']
        break;
      case 'list-reports':
        this.defaultKey = ['9']
        break;
      case 'new-account':
        this.defaultKey = ['11']
        break;
      case 'assign':
        this.defaultKey = ['12']
        break;
      case 'manage-index':
        this.defaultKey = ['7']
        break;
      default:
        this.defaultKey = ['0']
        break;
    }
  }

  handleSignOut(){
    localStorage.clear();
    this.$router.push('/login')
  }

  checkRole(listRole: string[]){

    return listRole.includes(this.user.roleName)
  }
}