import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { CourseDetail } from './course_detail';
import { sfrHttpClient } from './../../../api';

@Component({
  template: require('./view.html'),
  components: { 
    'course-detail': CourseDetail,
  },
})

export class InternCourses extends Vue {
  public dataSubject: any = {}
  public dataHandle: any = []
  public dataCourse: any = {}
  public Course: any
  public idSubject: any
  public checkShow = false
  public userData: any
  public DataX: any = []

  mounted() {
    this.getUser()
    this.getdataCourse()
  }

  async getUser() {
    this.userData = await sfrHttpClient.user.getCurrentUser()
  }

  async getdataCourse() {
    this.dataSubject = await sfrHttpClient.subjects.listSubject()
    this.dataCourse = await sfrHttpClient.user.getListCourse()
    const subjectList = []
    for (let j = 0; j < this.dataSubject.length; j++) {
      for (let i = 0; i < this.dataCourse.length; i++) {
        const objectSubject = {
          name: this.dataSubject[j].name,
          id: this.dataSubject[j].id,
          courses: [] as any,
        }
        if(subjectList.length) {
          for (let x = 0; x < subjectList.length; x++) {
            if (subjectList[x].id != this.dataCourse[i].subjectId && this.dataCourse[i].subjectId == this.dataSubject[j].id) {subjectList.push(objectSubject)}
          }
        } else {
          if (this.dataCourse[i].subjectId == this.dataSubject[j].id) {subjectList.push(objectSubject)}
        }
      }
    }

    for (let i = 0; i < this.dataCourse.length; i++) { 
      for (let j = 0; j < subjectList.length; j++) {
        const objectCourse = {
          title: this.dataCourse[i].title,
          id: this.dataCourse[i].id,
          courseData: {} as any,
        }
        if (this.dataCourse[i].subjectId == subjectList[j].id) {
          subjectList[j].courses.push(objectCourse)
        }
      }
    }

    this.dataHandle = subjectList
  }

  async clickSubject(data: any) {
    const subject = data.subject
    this.idSubject = data.index
    this.checkShow = true

    for (let i = 0; i < subject.courses.length; i++) {
      const params = {
        internId: this.userData.id,
        subjectId: subject.id,
        coursesId: subject.courses[i].id,
      }
      const getData = await sfrHttpClient.course.getOneCourseAssign(params)
      this.DataX.push(getData)
      subject.courses[i].courseData = getData.coursesData
      this.convertData(subject.courses[i].courseData)
    }
    // console.log('X: ',this.DataX)
  }

  convertData(data: any) {
    data.dayLearning = data.days
    // delete data.days
    for (let i = 0; i < data.dayLearning.length; i++) {
      data.dayLearning[i].children = data.dayLearning[i].lessions
      // delete data.dayLearning[i].lessions
      for (let j = 0; j < data.dayLearning[i].children.length; j++) {
        data.dayLearning[i].children[j].children = data.dayLearning[i].children[j].questions
        // delete data.dayLearning[i].children[0].questions
      } 
    }
  }
}