import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { CourseDay } from './day'
import { CourseLession } from './lession'
import 'he-tree-vue/dist/he-tree-vue.css'
import {Tree as _Tree, Draggable, Fold} from 'he-tree-vue'
import { sfrHttpClient } from 'src/api';

const Tree: any = _Tree;

@Component({
  template: require('./view.html'),
  name: 'course-detail',
  components: { 
    Tree: Tree.mixPlugins([Fold]),
    'day': CourseDay,
    'lession': CourseLession,
  },
  props: {
    Course: {
      type: Object
    },
    userData: {
      type: Object
    },
    DataX: {
      type: Object
    }
  },
})

export class CourseDetail extends Vue {
  public currPath = [0]
  public indexCurrDay = 0
  public indexLession = 0
  public indexQuiz = 0
  public ObjectView: any = this.$props.Course.dayLearning[0]
  public currentUser: any

  clickDay(data: any) {
    this.indexCurrDay = data.index
    this.currPath = data.path
    this.ObjectView = this.$props.Course.dayLearning[this.indexCurrDay]
  }
  clickLession(data: any) {
    this.indexLession = data.index
    this.currPath = data.path
    this.indexCurrDay = data.path[0]
    this.ObjectView = this.$props.Course.dayLearning[this.indexCurrDay].children[this.indexLession]
  }
}