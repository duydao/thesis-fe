import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import CKEditor from 'ckeditor4-vue'

Vue.use( CKEditor );

@Component({
  template: require('./view.html'),
  name: 'course-quiz',
  components: { 
    
  },
  props: {
    Quiz: {
      type: Array
    },
  },
})

export class CourseQuiz extends Vue {
  public form: any = {}

  // mounted() {
  //   this.checkData()
  // }

  // checkData() {
  //   console.log('.... ', this.$props.Quiz)
  // }

  handleSubmit(e: any) {
    e.preventDefault();
    this.form.validateFields((err: any, values: any) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }
}