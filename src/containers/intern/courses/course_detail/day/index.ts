import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { sfrHttpClient } from 'src/api';

@Component({
  template: require('./view.html'),
  name: 'course-day',
  props: {
    Course: {
      type: Object
    },
    DayLearning: {
      type: Object
    },
    DataX: {
      type: Object
    }
  },
})

export class CourseDay extends Vue {
  public visible = false
  public loading = false

  // mounted() {
  //   console.log('@@@ ', this.$props.DataX)
  // }

  showModal() {
    this.visible = true;
    console.log(this.$props.DayLearning.challenge)
  }

  handleCancel(e: any) {
    this.visible = false;
  }

  async handleOk(e: any) {
    console.log(this.$props.DataX)
    sfrHttpClient.course.submitAnswer(this.$props.DataX)
      .then(() => {
        this.$notification['success']({
          message: 'Success',
          description: 'Submit successful',
          duration: 2,
        });
      })
      .catch(e => {
        this.$message.error(e?.message || 'Error')
      })

    this.visible = false;
    this.loading = false;
  }
}