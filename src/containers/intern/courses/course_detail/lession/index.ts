import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { CourseQuiz } from '../quiz';
import { sfrHttpClient } from 'src/api';

@Component({
  template: require('./view.html'),
  name: 'course-lession',
  components: { 
    'quiz': CourseQuiz,
  },
  props: {
    Course: {
      type: Object
    },
    Lession: {
      type: Object
    },
    DataX: {
      type: Object
    }
  },
})

export class CourseLession extends Vue {
  public visible = false
  public loading = false
  
  mounted() {
    console.log('lession: ', this.$props.DataX)
  }

  showModal() {
    this.visible = true;
    console.log(this.$props.Lession)
  }

  async handleOk(e: any) {
    this.loading = true;
    const quiz = this.$props.Lession.children
    this.$props.Lession.totalScore = null

    for (let i = 0; i < quiz.length; i++) {
      if (quiz[i].type == 2) {
        const indexCorrect = quiz[i].answers.indexOf(quiz[i].answers.find((e: any) => e.isCorrect == true))
        quiz[i].checkCorrect = (quiz[i].checkCorrect == indexCorrect) ? true : false
      } else {
        quiz[i].answerIntern = quiz[i].answers[0].answer
        quiz[i].scoreValue = null
      }
    }

    sfrHttpClient.course.submitAnswer(this.$props.DataX)
      .then(() => {
        this.$notification['success']({
          message: 'Success',
          description: 'Submit successful',
          duration: 2,
        });
      })
      .catch(e => {
        this.$message.error(e?.message || 'Error')
      })
    
    this.visible = false;
    this.loading = false;
  }

  handleCancel(e: any) {
    this.visible = false;
  }
}