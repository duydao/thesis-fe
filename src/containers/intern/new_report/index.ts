import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { Report } from '../../../models/report'
import { FormModel } from 'ant-design-vue';
import moment from 'moment';
import { sfrHttpClient } from './../../../api';

Vue.use(FormModel)
@Component({
  template: require('./view.html'),
})


export class NewReport extends Vue {
  public moment: any

  private formNewReport: any = {}
  public labelCol: any = { span: 4 }
  public wrapperCol: any = { span: 18 }
  listMentors = []
  isLoading = false;
  form = { 
    date:  moment(new Date()),
    mentorId: null,
    isFisnish: true,
    reportContent: null
  }

  mounted() {
    this.getListMentor();
  }

  async handleNewReport() {
    const form: any = {...this.form}
    if(!form.mentorId || !form.reportContent){
      this.$message.error("Form invalid")
      return;
    }
    form.date =  moment(form.date).toDate();
    this.isLoading = true;

    sfrHttpClient.reports.createReport(form)
      .then(() => {
        this.$message.success('Report success');
        setTimeout(() => {
          this.$router.push('/reports')
        }, 1000)
      })
      .catch(e => {
        this.$message.error(e?.message || 'Error')
      })
      .finally(() => {
        this.isLoading = false;
      })

    // this.formNewReport.date = moment(this.formNewReport.birthday).format("DD-MM-YYYY")
  }

  disabledDate(current: any) {
    return current && current > moment().endOf('day');
  }

  private async getListMentor(){
    this.listMentors = await sfrHttpClient.users.getListMentor()
      .then((res) => {
        if(!!res.length){
          this.form.mentorId = res[0].id;
        }
        return res;
      })
      .catch(() => [])
  }
}