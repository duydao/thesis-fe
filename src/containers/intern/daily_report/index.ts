import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { Report } from '../../../models/report'
import { FormModel } from 'ant-design-vue';
import moment from 'moment';
import { sfrHttpClient } from './../../../api';

Vue.use(FormModel)
@Component({
  template: require('./view.html')
})


export class InternReport extends Vue {
  public moment: any
  public Reports: Report[] = []
  
  private report_content: any = null
  private formNewReport: any = {}
  public visible = false
  public labelCol: any = { span: 4 }
  public wrapperCol: any = { span: 18 }

  listMentor = [];
  reportDetail = null;

  formReport = { 
    date:  moment(new Date()),
    mentorId: null
  }

  private time: any;
  mounted() {
    this.getListMentor();
    this.timeoutData();
  }

  destroyed() {
    if(this.time) clearInterval(this.time)
  }

  clickDate(data: any) {
    this.report_content = data.report
  }

  showModal() {
    this.visible = true;
  }

  handleNewReport() {
    this.visible = false;
  }

  disabledDate(current: any) {
    return current && current > moment().endOf('day');
  }

  async getReport(){
    this.reportDetail = await sfrHttpClient.reports.getReportInter(this.formReport).catch(() => null)
  }

  private async getListMentor(){
    this.listMentor = await sfrHttpClient.users.getListMentor()
      .then((res) => {
        if(!!res.length){
          this.formReport.mentorId = res[0].id;
          this.getReport();
        }
        return res;
      })
      .catch(() => [])
  }

  private timeoutData(){
    this.time = setInterval(() => {
      if(this.formReport.mentorId){
        this.getReport();
      }
    }, 3000)
  }
  
}