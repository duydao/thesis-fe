import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { sfrHttpClient } from '../../../api';
import { calcProgress } from 'src/helpers/handle-data-index';
import moment from 'moment';

@Component({
  template: require('./view.html'),
})

export class InternDashBoard extends Vue {
  public dataModel: any = {}
  public data: any = {}
  public dataSubject: any = []
  public dataCourse: any = []
  public modelSubject: any = []
  public user: any = {}
  public listInfo: any = []
  public notiCourse: any = []
  public notiReport: any = []

  listMentor = [];
  reportDetail = [] as any;
  formReport = { 
    date:  moment(new Date()),
    mentorId: this.user.id
  }

  mounted() {
    this.callData()
    this.getListMentor()
  }

  async callData() {
    this.dataCourse = await sfrHttpClient.user.getListCourse()
    this.dataSubject = await sfrHttpClient.subjects.listSubject()
    this.user = await sfrHttpClient.user.getCurrentUser()
    this.createList()
    this.getAssign()
  }

  createList() {
    const subjectList = []
    for (let j = 0; j < this.dataSubject.length; j++) {
      for (let i = 0; i < this.dataCourse.length; i++) {
        const objectSubject = {
          name: this.dataSubject[j].name,
          id: this.dataSubject[j].id,
          courses: [] as any,
        }
        if(subjectList.length) {
          for (let x = 0; x < subjectList.length; x++) {
            if (subjectList[x].id != this.dataCourse[i].subjectId && this.dataCourse[i].subjectId == this.dataSubject[j].id) {subjectList.push(objectSubject)}
          }
        } else {
          if (this.dataCourse[i].subjectId == this.dataSubject[j].id) {subjectList.push(objectSubject)}
        }
      }
    }

    for (let i = 0; i < this.dataCourse.length; i++) { 
      for (let j = 0; j < subjectList.length; j++) {
        const objectCourse = {
          title: this.dataCourse[i].title,
          id: this.dataCourse[i].id,
          data: {} as any,
        }
        if (this.dataCourse[i].subjectId == subjectList[j].id) {
          subjectList[j].courses.push(objectCourse)
        }
      }
    }

    this.modelSubject = subjectList
  }

  async getAssign() {
    for (let i = 0; i < this.modelSubject.length; i++) {
      for (let j = 0; j < this.modelSubject[i].courses.length; j++) {
        const params = {
          internId: this.user.id,
          subjectId: this.modelSubject[i].id,
          coursesId: this.modelSubject[i].courses[j].id,
        }
        this.modelSubject[i].courses[j].data = await sfrHttpClient.course.getOneCourseAssign(params)
      }
    }
    for (let i = 0; i < this.modelSubject.length; i++) {
      for (let j = 0; j < this.modelSubject[i].courses.length; j++) {
        if (!this.modelSubject[i].courses[j].data.coursesData.dayLearning) {
          this.convertData(this.modelSubject[i].courses[j].data.coursesData)
        }
        calcProgress(this.modelSubject[i].courses[j].data)
      }
    }
    this.convertDataStatistical()
    this.getNoti()
  }

  async getNoti() {
    for (let i = 0; i < this.modelSubject.length; i++) {
      let checkCourseStop = false
      for (let j = 0; j < this.modelSubject[i].courses.length; j++) {
        const coursesData = this.modelSubject[i].courses[j].data.coursesData
        for (let k = 0; k < coursesData.dayLearning.length; k++) {
          if (!coursesData.dayLearning[k].isComplete) {
            this.notiCourse.push({
              content: `You need complete Day ${k+1}`,
              subject: this.modelSubject[i].courses[j].data.subjectId.name,
              course: this.modelSubject[i].courses[j].data.coursesId.title,
            })
            checkCourseStop = true
          }
          if (checkCourseStop) break
        }
      }
    }

    for (let i = 0; i < this.reportDetail.length; i++) {
      const name = this.reportDetail[i].mentorId.lastName + ' ' + this.reportDetail[i].mentorId.firstname
      const date = this.reportDetail[i].date.toLocaleString().split(',')[0]
      this.notiReport.push({
        content: `${name} (mentor) has reply your daily report (${date}). Please read this reply.`
      })
    }
    console.log('report: ', this.notiCourse)
  }

  convertData(data: any) {
    data.dayLearning = data.days
    // delete data.days
    for (let i = 0; i < data.dayLearning.length; i++) {
      data.dayLearning[i].children = data.dayLearning[i].lessions
      // delete data.dayLearning[i].lessions
      for (let j = 0; j < data.dayLearning[i].children.length; j++) {
        data.dayLearning[i].children[j].children = data.dayLearning[i].children[j].questions
        // delete data.dayLearning[i].children[0].questions
      } 
    }
  }

  convertDataStatistical() {
    for (let i = 0; i < this.modelSubject.length; i++) {
      for (let j = 0; j < this.modelSubject[i].courses.length; j++) {
        this.listInfo.push({
          subject: this.modelSubject[i].name,
          course: this.modelSubject[i].courses[j].title,
          isCompleted: this.modelSubject[i].courses[j].data.coursesData.progress ? (this.modelSubject[i].courses[j].data.coursesData.isCompleted ? "Completed" : "In Progress") : "Pending",
          start_day: this.modelSubject[i].courses[j].data.coursesData.progress ? (new Date(this.modelSubject[i].courses[j].data.createdAt).toLocaleString().split(',')[0]) : "Pending",
          progress: this.modelSubject[i].courses[j].data.coursesData.progress || 0,
          gpa: this.modelSubject[i].courses[j].data.coursesData.gpa || 0,
          knowledge: this.modelSubject[i].courses[j].data.coursesData.knowledge || 0,
          recommended: this.modelSubject[i].courses[j].data.coursesData.recommended || 0,
          subIndex: this.modelSubject[i].courses[j].data.coursesData.subIndexTotal
        })
      }
    }
  }

  private async getListMentor(){
    this.listMentor = await sfrHttpClient.users.getListMentor()
      .then((res) => {
        if(!!res.length){
          for (let i = 0; i < res.length; i++) {
            this.formReport.mentorId = res[0].id;
            this.getReport();
          }
        }
        return res;
      })
      .catch(() => [])
  }

  async getReport(){
    this.reportDetail.push(await sfrHttpClient.reports.getReportInter(this.formReport).catch(() => null))
  }
}
