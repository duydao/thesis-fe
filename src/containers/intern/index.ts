import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';

@Component({
  template: require('./view.html'),
})

export class ListInterns extends Vue {}