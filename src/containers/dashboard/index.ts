import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';

@Component({
  template: require('./view.html'),
})

export class Dashboard extends Vue {
  mounted() {
    this.checkPermission();
  }

  private checkPermission(){
    const user = JSON.parse(localStorage.getItem('@user') || '{}');
    console.log(user);
    switch(user.roleName){
      case 'Intern':
        this.$router.push('/home_inter')
        break;
      case 'Mentor':
        this.$router.push('/home');
        break;
      default: break;
    }
  }
}