import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { sfrHttpClient } from './../../api';

@Component({
  template: require('./view.html'),
})

export class Statistical extends Vue {
  public data: any = []
  public dataModel = {
    subject: "" as any,
    course: "" as any,
    list_mentees: [] as any
  }
  public id_element: any = 0
  public visible = false
  public mentee: any = {}

  public columns = [
    {
      title: 'name',
      dataIndex: 'name',
      width: '30%',
    },
    {
      title: 'startday',
      dataIndex: 'start_day',
      width: '15%',
    },
    {
      title: 'progress',
      dataIndex: 'progress',
      width: '15%',
    },
    {
      title: 'gpa',
      dataIndex: 'gpa',
    },
    {
      title: 'knowledge',
      dataIndex: 'knowledge',
    },
    {
      title: 'recommended',
      dataIndex: 'recommended',
    },
  ];

  mounted() {
    this.callData();
    this.clickDetail();
  }

  async callData() {
    this.dataModel.subject = this.$route.query.subjectName
    this.dataModel.course = this.$route.query.coursesName
    const params = {
      subjectId: this.$route.query.subjectId,
      coursesId: this.$route.query.coursesId
    }
    this.data = await sfrHttpClient.reports.getStatistical(params)
    this.convertDataStatistical(this.data)
    console.log('______ ', this.data)
  }

  convertDataStatistical(data: any) {
    for (let i = 0; i < data.length; i++) {
      this.dataModel.list_mentees.push({
        key: i,
        name: data[i].internId.lastName + " " + data[i].internId.firstname,
        start_day: new Date(this.data[i].createdAt).toLocaleString().split(',')[0],
        progress: data[i].coursesData.progress,
        gpa: data[i].coursesData.gpa,
        knowledge: data[i].coursesData.knowledge,
        recommended: data[i].coursesData.recommended,
        subIndex: data[i].coursesData.subIndexTotal,
        isCompleted: data[i].coursesData.isCompleted ? "Completed" : "In Progress"
      })
    }
  }

  clickDetail() {
    const targetElement = document.getElementById('table_statistical') as HTMLDivElement
    targetElement.addEventListener('click', (e) => {
      const childElement = e.target as HTMLDivElement
      const parentElement = childElement.parentElement
      this.id_element = parentElement?.getAttribute('data-row-key')
      this.mentee = this.dataModel.list_mentees[this.id_element]
      this.visible = true;
    })
  }

  handleCancel() {
    this.visible = false;
  }
}