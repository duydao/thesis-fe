import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';

@Component({
  template: require('./view.html'),
})

export class StatisticalDetail extends Vue {
  private data = {
    name: 'Nguyen Van A',
    name_subject: 'web',
    name_course: 'vuejs',
    start_day: '30/01/2020',
    progress: 60,
    recommended: 60,
    completion: 8.5,
    gpa: 7.5,
    knowledge: 8,
    sub_indexs: [
      {
        name: 'team work',
        score: 8,
      },
      {
        name: 'diligence',
        score: 8,
      },
      {
        name: 'attitude',
        score: 8,
      },
    ],
    course: [
      {
        key: '0',
        part: 'Day 1',
        time_completion: 7,
        time_complete: 6.15,
        completion: 10,
        score: 7,
        type: 'knowledge test'
      },
      {
        key: '1',
        part: 'Day 1 - Lession 1',
        time_completion: 7,
        time_complete: 6.15,
        completion: 10,
        score: 7,
        type: 'lession test'
      },
      {
        key: '2',
        part: 'Day 1 - Lession 2',
        time_completion: 7,
        time_complete: 6.15,
        completion: 10,
        score: 7,
        type: 'lession test'
      },
      {
        key: '3',
        part: 'Day 1 - Lession 3',
        time_completion: 7,
        time_complete: 6.15,
        completion: 10,
        score: 7,
        type: 'lession test'
      },
      {
        key: '4',
        part: 'Day 2',
        time_completion: 7,
        time_complete: 6.15,
        completion: 10,
        score: 7,
        type: 'knowledge test'
      },
      {
        key: '5',
        part: 'Day 2 - Lession 1',
        time_completion: 7,
        time_complete: 6.15,
        completion: 10,
        score: 7,
        type: 'lession test'
      },
    ]
  }

  private columns = [
    {
      title: 'part',
      dataIndex: 'part',
      width: '25%',
      scopedSlots: { customRender: 'part' },
    },
    {
      title: 'Completion Time',
      dataIndex: 'time_completion',
      scopedSlots: { customRender: 'time_completion' },
    },
    {
      title: 'Complete In',
      dataIndex: 'time_complete',
      scopedSlots: { customRender: 'time_complete' },
    },
    {
      title: 'Completion Score',
      dataIndex: 'completion',
      scopedSlots: { customRender: 'completion' },
    },
    {
      title: 'Score',
      dataIndex: 'score',
      scopedSlots: { customRender: 'score' },
    },
    {
      title: 'Type',
      dataIndex: 'type',
      scopedSlots: { customRender: 'type' },
    },
  ]

}