import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { FormConfigCourse } from '../form_config_course'
import { ActionType } from '../../../store';
import { sfrHttpClient } from './../../../api';

@Component({
  template: require('./view.html'),
  components: { 
    'config-course': FormConfigCourse,
  },
})

export class EditCourses extends Vue {
  public formData: any = null
  public idCourse: any = null

  mounted() {
    this.receiveData()
  }

  async receiveData() {
    this.idCourse = this.$route.fullPath.split('/')[2]
    this.formData = await sfrHttpClient.course.getCourse(this.idCourse)
    console.log('check api: ', this.formData)
    this.convertData(this.formData)
  }

  convertData(data: any) {
    data.subjectId = data.subject.id
    data.dayLearning = data.days
    delete data.days
    for(let i = 0; i < data.dayLearning.length; i++) {
      data.dayLearning[i].children = data.dayLearning[i].lessions
      delete data.dayLearning[i].lessions
      for(let j = 0; j < data.dayLearning[i].children.length; j++) {
        data.dayLearning[i].children[j].children = data.dayLearning[i].children[j].questions
        delete data.dayLearning[i].children[j].questions
      }
    }
  }
}