import Vue from 'vue'
import Component from 'vue-class-component'
import './style.scss'
import CKEditor from 'ckeditor4-vue'

Vue.use( CKEditor );

@Component({
  template: require('./view.html'),
  name: 'answer',
  props: {
    answers: {
      type: Array
    },
  },
})

export class Answer extends Vue {
  public answer = this.$props.answers[0].answer 
  public correct = this.$props.answers[0].isCorrect
}
