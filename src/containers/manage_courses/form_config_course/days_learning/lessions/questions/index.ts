import Vue from 'vue'
import Component from 'vue-class-component'
import './style.scss'
import CKEditor from 'ckeditor4-vue'
import { Answer } from './answer'
import { TypeQuestion } from '../../../../../../models/courses'

Vue.use( CKEditor );

@Component({
  template: require('./view.html'),
  name: 'question',
  components: { 'answer': Answer },
  props: {
    currPath: {
      type: Array
    },
    indexLession: {
      type: Number
    },
    lession: {
      type: Object
    },
    indexCurrDay: {
      type: Number
    },
    indexQuestion: {
      type: Number
    },
    question: {
      type: Object
    },
  },
})

export class Questions extends Vue {

  public enumType = Object.keys(TypeQuestion).filter((key: any) => !isNaN(Number(TypeQuestion[key])));

  typeOfQuestion(index: any) {
    this.$props.question.type = index
    if (this.$props.question.type == 0) {
      this.$props.question.answers = this.$props.question.answers.slice(0,1)
    }
    else if (this.$props.question.type == 2) {
      for(let i = this.$props.question.answers.length; i < 2; i++) {
        this.$props.question.answers.push({ // Answer
          answer: '',
          isCorrect: false,
        });
      }
    }
  }

  removeAnswer(item: Answer) {
    const indexAnswer = this.$props.question.answers.indexOf(item);
    if (indexAnswer !== -1) {
      this.$props.question.answers.splice(indexAnswer, 1);
    }
  }
  addAnswer() {
    this.$props.question.answers.push({ // Answer
      answer: '',
      isCorrect: false,
    });
  }
}
