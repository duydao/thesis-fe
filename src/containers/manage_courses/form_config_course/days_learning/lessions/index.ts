import Vue from 'vue'
import Component from 'vue-class-component'
import './style.scss'
import CKEditor from 'ckeditor4-vue'
import { Questions } from './questions'

Vue.use( CKEditor );

@Component({
  template: require('./view.html'),
  name: 'lession',
  components: { 'question': Questions },
  props: {
    currPath: {
      type: Array
    },
    indexLession: {
      type: Number
    },
    lession: {
      type: Object
    },
    indexCurrDay: {
      type: Number
    },
    isRemoveLession: {
      type: Boolean
    },
    indexQuestion: {
      type: Number
    },
    question: {
      type: Object
    },
  },
})

export class Lessions extends Vue {
  
}
