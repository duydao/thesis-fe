import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { Lessions } from './lessions'
import CKEditor from 'ckeditor4-vue';
import { TypeChallenge, TypeQuestion } from '../../../../models/courses'

Vue.use( CKEditor );

@Component({
  template: require('./view.html'),
  name: 'day-learning',
  components: { 'lession': Lessions },
  props: {
    currPath: {
      type: Array
    },
    indexCurrDay: {
      type: Number
    },
    dayLearning: {
      type: Object
    },
    indexLession: {
      type: Number
    },
    lession: {
      type: Object
    },
    isRemoveDay: {
      type: Boolean
    },
    indexQuestion: {
      type: Number
    },
    question: {
      type: Object
    },
  },
})

export class DaysLearning extends Vue {
  public enumType = Object.keys(TypeChallenge).filter((key: any) => !isNaN(Number(TypeChallenge[key])));

  typeOfChallenge(index: any) {
    this.$props.dayLearning.challenge.typeCh = index
  }
}
