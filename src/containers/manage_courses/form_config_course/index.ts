import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { DayLearning, Lession, Question, Answer } from '../../../models/courses'
import { DaysLearning } from './days_learning'
import CKEditor from 'ckeditor4-vue';
import 'he-tree-vue/dist/he-tree-vue.css';
import {Tree as _Tree, Draggable, Fold} from 'he-tree-vue';
import { ActionType } from '../../../store';
import { sfrHttpClient } from './../../../api';
import { isNull } from 'lodash';

const Tree: any = _Tree;
Vue.use( CKEditor );
@Component({
  name: 'FormConfigCourse',
  template: require('./view.html'),
  components: { 
    'day-learning': DaysLearning,
    Tree: Tree.mixPlugins([Draggable, Fold]),
  },
  props: {
    formData: {
      type: Object
    },
    idSubject: {
      type: Array
    }
  }
})

export class FormConfigCourse extends Vue {
  public listDataSubject: any = []
  public listDataCourse: any = []
  public listNameCourse: any = []
  public formNewData: any = this.$props.formData
  public listSubject: any = []
  public indexSubject: any
  public currPath = [0]
  public indexCurrDay = 0
  public indexLession = 0
  public indexQuestion = 0
  public errorsConfigCourse = ['']
  public expand = true
  public originIndexSubject: any = 0
  public isCreate = false

  mounted() {
    this.getListSubject()
  }

  async getListSubject() {
    const subject = await sfrHttpClient.subjects.listSubject()
      .catch(() => []);
    this.listDataSubject = subject;
    if (!this.$props.formData.subjectId) this.$props.formData.subjectId = subject[0].id
    this.originIndexSubject = subject.indexOf(subject.find((ele: any) => ele.id == this.$props.formData.subjectId))
    this.isCreate = (this.$route.params.id == null) ? true : false
  }

  clickCollapse() {
    const icon = document.getElementsByClassName('iconCollapse') as HTMLCollectionOf<HTMLElement>
    if (this.expand == true) {
      this.expand = false
      icon[0].style.transform = "rotate(0deg)"
    }
    else {
      this.expand = true
      icon[0].style.transform = "rotate(90deg)"
    }
  }

  async clickSubject(index: any) {
    this.formNewData.subjectId = this.listDataSubject[index].id
    this.indexSubject = index
    await sfrHttpClient.course.getCoursesListOfSubject(this.formNewData.subjectId)
      .then(r => {
        this.listDataCourse = r
      })
      .catch( e => {
        this.$message.error(e.message)
      })
    this.listNameCourse = this.listDataCourse.map((t: any) => t.title)
  }

  clickDay(data: any) {
    this.indexCurrDay = data.index
    this.currPath = data.path
  }
  clickLession(data: any) {
    this.indexLession = data.index
    this.currPath = data.path
    this.indexCurrDay = data.path[0]
  }
  clickQuestion(data: any){
    this.indexQuestion = data.index
    this.currPath = data.path
    this.indexLession = data.path[1]
    this.indexCurrDay = data.path[0]
  }

  removeDay(item: DayLearning) {
    const index = this.formNewData.dayLearning.indexOf(item);
    if (index !== -1) {
      if (this.indexCurrDay == (this.formNewData.dayLearning.length - 1)) {
        this.indexCurrDay = this.formNewData.dayLearning.length - 2
        this.currPath = [this.formNewData.dayLearning.length - 2]
      }
      this.formNewData.dayLearning.splice(index, 1);
    }
  }
  addDay() {
    this.formNewData.dayLearning.push({
      name: '',
      description: '',
      // time: 50,
      challenge: {
        status: true,
        typeCh: 1,
        topic: '',
      },
      children: [{ // Lession
        name: '',
        description: '',
        time: 10,
        children: [{ // Question
          name: '',
          type: 0,
          score: 1,
          answers: [{
            answer: 'No answer',
            isCorrect: false,
          }]
        }]
      }],
    });
  }

  removeLession(item: Lession) {
    const indexLession = this.formNewData.dayLearning[this.indexCurrDay].children.indexOf(item);
    if (indexLession !== -1) {
      if (this.indexLession == (this.formNewData.dayLearning[this.indexCurrDay].children.length - 1)) {
        this.indexLession = this.formNewData.dayLearning[this.indexCurrDay].children.length - 2
        this.currPath = [this.indexCurrDay, this.formNewData.dayLearning[this.indexCurrDay].children.length - 2]
      }
      this.formNewData.dayLearning[this.indexCurrDay].children.splice(indexLession, 1);
    }
  }
  
  addLession() {
    this.formNewData.dayLearning[this.indexCurrDay].children.push({ // Lession
      name: '',
      description: '',
      time: 10,
      children: [{ // Question
        name: '',
        type: 0,
        score: 1,
        answers: [{
          answer: 'No answer',
          isCorrect: false,
        }]
      }]
    });
  }

  removeQuestion(item: Question) {
    const indexQuestion = this.formNewData.dayLearning[this.indexCurrDay].children[this.indexLession].children.indexOf(item);
    if (indexQuestion !== -1) {
      if (this.indexQuestion == (this.formNewData.dayLearning[this.indexCurrDay].children[this.indexLession].children.length - 1)) {
        this.indexQuestion = this.formNewData.dayLearning[this.indexCurrDay].children[this.indexLession].children.length - 2
        this.currPath = [this.indexCurrDay, this.indexLession, this.$props.Course.dayLearning[this.indexCurrDay].children[this.indexLession].children.length - 2]
      }
      this.formNewData.dayLearning[this.indexCurrDay].children[this.indexLession].children.splice(indexQuestion, 1);
    }
  }
  addQuestion() {
    this.formNewData.dayLearning[this.indexCurrDay].children[this.indexLession].children.push({ // Question
      name: '',
      type: 0,
      score: 1,
      answers: [{
        answer: 'No answer',
        isCorrect: false,
      }]
    });
  }

  // CHECK FORM
  async onSubmit() {
    this.errorsConfigCourse = []
    let checkCourse = true
    const DayL = this.formNewData.dayLearning // def Days
    let checkDay = true
    let checkLess = true
    let checkQues = true
    let checkAnsw = true
    let lengLessbefore = 0
    let lengQuesBefore = 0
    let checkAnsCorrect = 0

    this.removeAllClass("error")
    this.removeAllClass("errorNodeTree")
    if (this.formNewData.subjectId) {
      if (this.formNewData.title) {
        if (!this.listNameCourse.includes(this.formNewData.title)) {
          if (this.formNewData.description) {
            for(let iDay = 0; iDay < DayL.length; iDay++) { // load eachDay
              if (DayL[iDay].name && DayL[iDay].description) { // check Day Learning
                const Less = DayL[iDay].children // def Lession
                if(iDay > 0) lengLessbefore += DayL[iDay-1].children.length
                for(let iLess = 0; iLess < Less.length; iLess++) { // load eachLession
                  if (Less[iLess].name && Less[iLess].description) { // check Lession
                    const Ques = Less[iLess].children // def Questions
                    if(iDay == 0) {
                      if(iLess > 0) lengQuesBefore += DayL[0].children[iLess-1].children.length
                    }
                    else {
                      if(iLess > 0) {
                        lengQuesBefore += DayL[iDay].children[iLess-1].children.length
                      }
                      else {
                        lengQuesBefore += DayL[iDay-1].children[DayL[iDay-1].children.length - 1].children.length
                      }
                    }
                    for(let iQues = 0; iQues < Ques.length; iQues++) { // load eachQuestion
                      if (Ques[iQues].name) { // check question name
                        if (Ques[iQues].type == 2) { // check question's type MULTIPLE
                          const Answ = Ques[iQues].answers // def Answers
                          if (Answ.length > 1) {
                            for(let iAns = 0; iAns < Answ.length; iAns++){
                              if (!Answ[iAns].answer) {
                                document.getElementsByClassName("question")[lengQuesBefore + iQues].classList.add("errorNodeTree")
                                this.showNoti("Day " + (iDay + 1) + " - Lession " + (iLess + 1) + " - Quession " + (iQues + 1) + ": Please enter all Answer fields")
                                checkDay = false
                                checkLess = false
                                checkQues = false
                                checkAnsw = false
                                break
                              }
                              else {
                                if (Answ[iAns].isCorrect == true) {
                                  checkAnsCorrect = checkAnsCorrect + 1
                                }
                                checkAnsw = true
                              }
                            }
                          }
                          else {
                            document.getElementsByClassName("question")[lengQuesBefore + iQues].classList.add("errorNodeTree")
                            this.showNoti("Day " + (iDay + 1) + " - Lession " + (iLess + 1) + " - Quession " + (iQues + 1) + ": Multiple choice questions must have AT LEAST 2 answers")
                            checkDay = false
                            checkLess = false
                            checkQues = false
                            checkAnsw = false
                            break
                          }
                          if (checkAnsw) {
                            if (checkAnsCorrect == 0) {
                              document.getElementsByClassName("question")[lengQuesBefore + iQues].classList.add("errorNodeTree")
                              this.showNoti("Day " + (iDay + 1) + " - Lession " + (iLess + 1) + " - Quession " + (iQues + 1) + ": Question must have 1 correct answer")
                              checkDay = false
                              checkLess = false
                              checkQues = false
                              checkAnsw = false
                              break
                            }
                            else if (checkAnsCorrect > 1) {
                              document.getElementsByClassName("question")[lengQuesBefore + iQues].classList.add("errorNodeTree")
                              this.showNoti("Day " + (iDay + 1) + " - Lession " + (iLess + 1) + " - Quession " + (iQues + 1) + ": Question has ONLY 1 correct answer")
                              checkDay = false
                              checkLess = false
                              checkQues = false
                              checkAnsw = false
                              break
                            }
                            else {
                              checkAnsw = true
                            }
                          }
                        }
                        if (checkAnsw) {
                          // ____________________________ FINAL SUCCESS ____________________________
                          this.convertDataObject()
                          console.log('___ check', this.formNewData)
                          if (this.isCreate) {
                            await sfrHttpClient.course.createCourses(this.formNewData)
                            .then(r => {
                              this.$notification['success']({
                                message: 'Successful',
                                description: `Create new course "${this.formNewData.title}" Successful`,
                                duration: 2,
                              });
                              setTimeout(() => {
                                this.$router.push('/manage-courses');
                              }, 1000);
                            })
                            .catch(e => {
                              this.showNoti(e.message)
                            })
                          } else {
                            await sfrHttpClient.course.UpdateCourse(this.formNewData, this.$route.params.id)
                            .then(r => {
                              this.$notification['success']({
                                message: 'Successful',
                                description: `Update course "${this.formNewData.title}" Successful`,
                                duration: 2,
                              });
                              setTimeout(() => {
                                this.$router.push('/manage-courses');
                              }, 1000);
                            })
                            .catch(e => {
                              this.showNoti(e.message)
                            })
                          }
                          
                        }
                      }
                      else {
                        document.getElementsByClassName("question")[lengQuesBefore + iQues].classList.add("errorNodeTree")
                        this.showNoti("Day " + (iDay + 1) + " - Lession " + (iLess + 1) + " - Quession " + (iQues + 1) + ": Please enter Question")
                        checkDay = false
                        checkLess = false
                        checkQues = false
                        break
                      }
                    }
                    if (!checkQues) break
                  }
                  else {
                    document.getElementsByClassName("lession")[lengLessbefore + iLess]?.classList.add("errorNodeTree")
                    this.showNoti("Day " + (iDay + 1) + " - Lession " + (iLess + 1) + ": Please enter Lession's name and Lesion's description")
                    checkDay = false
                    checkLess = false
                    break
                  }
                }
                if (!checkLess) break
              }
              else {
                document.getElementsByClassName("day")[iDay].classList.add("errorNodeTree")
                this.showNoti("Day " + (iDay + 1) + ": Please enter Day's name and Day's description")
                checkCourse = false
                break
              }
              if (!checkDay) break
            }
          }
          else this.showNoti("Please enter Course's description")
        }
        else {
          this.showNoti("Course's Name already exists in subject " + this.formNewData.title)
        }
      }
      else {
        // this.removeAllClass("error")
        document.getElementsByClassName("course-name")[0]?.classList.add("error")
        this.showNoti("Please enter Course's name and Course's description")
      }
    }
    else {
      document.getElementsByClassName("subject-name")[0]?.getElementsByClassName("ant-select-selection")[0].classList.add("error")
      this.showNoti("Please choose Course's subject")
    }
  }

  removeAllClass(className: any) {
    const listClassName = document.getElementsByClassName(className)
    const lengthList = listClassName.length
    for(let i = 0; i < lengthList; i ++) {
      listClassName[i].classList.remove(className)
    }
  }
  showNoti(noti: any) {
    this.errorsConfigCourse.push(noti)
    this.$notification['error']({
      message: 'Error',
      description: this.errorsConfigCourse[0],
      duration: 5,
    });
  }

  convertDataObject() {
    this.formNewData.days = this.formNewData.dayLearning
    delete this.formNewData.dayLearning
    delete this.formNewData.subject

    for(let i = 0; i < this.formNewData.days.length; i++) {
      this.formNewData.days[i].lessions = this.formNewData.days[i].children
      delete this.formNewData.days[i].children
      for(let j = 0; j < this.formNewData.days[i].lessions.length; j++) {
        this.formNewData.days[i].lessions[j].questions = this.formNewData.days[i].lessions[j].children
        delete this.formNewData.days[i].lessions[j].children
        this.formNewData.days[i].lessions[j].time = Number(this.formNewData.days[i].lessions[j].time)
      }
    }

    this.formNewData.time = Number(this.formNewData.days.length)
  }
}

