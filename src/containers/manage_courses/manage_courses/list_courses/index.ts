import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { Subject, Course } from 'models/courses'
import { FormConfigCourse } from '../../form_config_course'
import { sfrHttpClient } from 'src/api';

@Component({
  template: require('./view.html'),
  name: 'list-courses',
  components: { 
    'config-course': FormConfigCourse,
  },
  props: {
    subject: {
      type: Object
    },
    indexCurrSubject: {
      type: Number
    }
  },
})

export class ListCourses extends Vue {
  async deleteCourse(data: any) {
    const courseName = data.course.title
    await sfrHttpClient.course.deleteCourse(data.course.id)
    .then(r => {
      this.$notification['success']({
        message: 'Successful',
        description: `delete course "${courseName}" successful`,
        duration: 2,
      });
      window.location.reload();
    })
  }

  editCourse(data: any) {
    const path = `/course/${data.course.id}`
    this.$router.push(path)
  }
}
