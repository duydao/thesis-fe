import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { ListCourses } from './list_courses';
import { ActionType } from '../../../store';
import { sfrHttpClient } from './../../../api';
import { createTupleTypeNode, TupleType } from 'typescript';

@Component({
  template: require('./view.html'),
  components: { 
    'list-courses': ListCourses,
  }
})

export class ManageCourses extends Vue {
  
  mounted() {
    this.getListSubject();
  }

  public Subject: any = []
  public visible = false;
  public visibleEdit = false;
  public currSubject: any = null
  public indexCurrSubject: any = null
  public objNewSubject = {
    name: '',
    courses: [],
  }
  public dataSubjectName: any
  public errorsNewSubject = ['']
  public storeListCourse: any = []

  formEditSubject = {
    id: '',
    name: '',
  }

  showModal() {
    this.visible = true;
    this.errorsNewSubject = []
  }
  handleCancel() {
    this.visible = false;
  }

  async getListSubject(){
    const subject = await sfrHttpClient.subjects.listSubject()
      .catch(() => []);
    this.Subject = subject;
    for(let i = 0; i < subject.length; i++) {
      const itemSubject = await sfrHttpClient.subjects.getCoursesBySubject(subject[i].id).catch(() => [])
      this.storeListCourse.push(itemSubject)
    }
    this.dataSubjectName = this.Subject.map((t: any)=>t.name.toLowerCase());
  }

  async clickSubject(data: any, e: any) {
    this.currSubject = data.subject
    this.indexCurrSubject = data.index
    this.currSubject.courses = this.storeListCourse[data.index]
    const ele = Array.from(document.getElementsByClassName('list-course') as HTMLCollectionOf<HTMLElement>)
    ele.forEach((element) => {
      if(element.style.display == 'block') {
        element.style.display = 'none'
      }
      else {
        element.style.display = 'block'
      }
    })
  }

  async checkNewSubject() {
    this.objNewSubject.name = this.objNewSubject.name.toLowerCase()
    if (this.objNewSubject.name) {
      let check = 0
      for(let i=0; i <= this.dataSubjectName.length; i++) {
        if (this.objNewSubject.name != this.dataSubjectName[i]) {
          check = 0
        }
        else {
          check = 1
          this.errorsNewSubject = ["This subject's name is already exists"]
          break
        }
      }
      if (check == 0) {
        this.visible = false;

        await sfrHttpClient.subjects.createSubject({name: this.objNewSubject.name})
          .then(r => {
            this.$notification['success']({
              message: 'Successful',
              description: 'Create subject "' + this.objNewSubject.name + '" successful',
              duration: 5,
            });

            this.Subject.push({
              id: r.id,
              name: this.objNewSubject.name,
              courses: [],
            });

            this.getListSubject();
          })
          .catch(e => {
            this.$notification['error']({
              message: 'Error',
              description: e.message,
              duration: 5,
            });
          })
        this.objNewSubject.name = ''
      }
    }
    else this.errorsNewSubject = ["Subject's Name is required"]
  }

  editSubject(data: any){
    this.formEditSubject.id = data.id;
    this.formEditSubject.name = data.name;
    this.visibleEdit = true;
  }

  handleCancelEdit(){
    this.formEditSubject.id = '';
    this.formEditSubject.name = ''
    this.visibleEdit = false;
  }

  async editSubjectSubmit(){
    const {name, id} = {...this.formEditSubject};
    this.handleCancelEdit();
    await sfrHttpClient.subjects.editSubject({name}, id)
      .then(r => {
        this.$notification['success']({
          message: 'Successful',
          description: 'Edit subject "' + r.name + '" successful',
          duration: 5,
        });
        this.Subject =  this.Subject.map((it: any) => {
          if(it.id === id) it.name = name
          return it;
        })
        console.log(r)
      })
      .catch(e => {
        this.$notification['error']({
          message: 'Error',
          description: e.message,
          duration: 5,
        });
      })


  }

  async deleteSubject(data: any) {
    const SubjectName = data.subject.name
    await sfrHttpClient.subjects.deleteSubject(data.subject.id)
    .then(r => {
      this.$notification['success']({
        message: 'Successful',
        description: `delete subject "${SubjectName}" successful`,
        duration: 2,
      });
      window.location.reload();
    })
  }
}