import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { Subject, TypeChallenge, TypeQuestion } from '../../../models/courses';
import { FormConfigCourse } from '../form_config_course';
import { ActionType } from '../../../store';
import { sfrHttpClient } from './../../../api';

@Component({
  template: require('./view.html'),
  components: { 
    'config-course': FormConfigCourse,
  },
})

export class NewCourse extends Vue {
  public currentUser: any = 0;
  public idSubject: any = null;

  mounted() {
    this.getCurrentUser()
  }

  async getCurrentUser() {
    this.currentUser = await sfrHttpClient.user.getCurrentUser()
      .catch(() => []);
    this.formData.authorId = this.currentUser.id
  }
  
  public formData: any = {
    title: '',
    time: 1,
    authorId: this.currentUser.id,
    description: '',
    subjectId: this.idSubject,
    dayLearning: [{
      name: '',
      description: '',
      // time: 50,
      challenge: {
        status: true,
        typeCh: 1,
        topic: '',
      },
      children: [{ // Lession
        name: '',
        description: '',
        time: 10,
        children: [{ // Question
          name: '',
          type: 2,
          score: 1,
          answers: [
            {
              answer: 'No answer',
              isCorrect: false,
            },
            {
              answer: 'No answer',
              isCorrect: false,
            }
          ]
        }]
      }],
    }],
  }

}