import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { CourseDay } from './day'
import { CourseLession } from './lession';
import { CourseQuiz } from './quiz';
import 'he-tree-vue/dist/he-tree-vue.css'
import {Tree as _Tree, Draggable, Fold} from 'he-tree-vue'
import { TypeChallenge, TypeQuestion } from '../../../models/courses'
import { sfrHttpClient } from './../../../api';

const Tree: any = _Tree;

@Component({
  template: require('./view.html'),
  components: { 
    Tree: Tree.mixPlugins([Fold]),
    'day': CourseDay,
    'lession': CourseLession,
    'quiz': CourseQuiz,
  },
})

export class MentorCourses extends Vue {
  public userData: any
  public dataSubject: any = []
  public dataCourse: any = []
  public dataHandle: any = []
  public listIntern: any = []
  public IDInternParam: any
  public IDCourseParam: any
  public IDSubjectParam: any
  public checkShow = false
  public dataGetOneAssign: any = {}
  public Course: any = {}

  public currPath = [0]
  public indexCurrDay = 0
  public indexLession = 0
  public indexQuiz = 0
  public ObjectView: any

  mounted() {
    this.getUser()
    this.getdataCourse()
  }

  async getUser() {
    this.userData = await sfrHttpClient.user.getCurrentUser()
  }

  async getdataCourse() {
    this.listIntern = await sfrHttpClient.user.getListIntern()
    this.dataSubject = await sfrHttpClient.subjects.listSubject()
    this.dataCourse = await sfrHttpClient.user.getListCourse()
    
    const subjectList = []
    for (let j = 0; j < this.dataSubject.length; j++) {
      for (let i = 0; i < this.dataCourse.length; i++) {
        const objectSubject = {
          name: this.dataSubject[j].name,
          id: this.dataSubject[j].id,
          courses: [] as any,
        }
        if(subjectList.length) {
          for (let x = 0; x < subjectList.length; x++) {
            if (subjectList[x].id != this.dataCourse[i].subjectId && this.dataCourse[i].subjectId == this.dataSubject[j].id) {subjectList.push(objectSubject)}
          }
        } else {
          if (this.dataCourse[i].subjectId == this.dataSubject[j].id) {subjectList.push(objectSubject)}
        }
      }
    }

    for (let i = 0; i < this.dataCourse.length; i++) { 
      for (let j = 0; j < subjectList.length; j++) {
        const objectCourse = {
          title: this.dataCourse[i].title,
          id: this.dataCourse[i].id,
          courseData: {} as any,
        }
        if (this.dataCourse[i].subjectId == subjectList[j].id) {
          subjectList[j].courses.push(objectCourse)
        }
      }
    }
    this.dataHandle = subjectList
  }

  async clickMentee(data: any) {
    this.IDInternParam = data.mentee.id
    this.callAPI()
  }

  async clickSubject(data: any) {
    this.IDSubjectParam = data.subject.id
    this.callAPI()
  }

  async clickCourse(data: any) {
    this.IDCourseParam = data.course.id
    this.callAPI()
  }

  async callAPI() {
    if (this.IDInternParam && this.IDSubjectParam && this.IDCourseParam) {
      const params = {
        internId: this.IDInternParam,
        subjectId: this.IDSubjectParam,
        coursesId: this.IDCourseParam,
      }
      this.dataGetOneAssign = await sfrHttpClient.course.getOneCourseAssign(params)
      this.Course = this.dataGetOneAssign.coursesData
      this.convertData(this.Course)
      this.ObjectView = this.Course.dayLearning[0]

      this.checkShow = true
    }
  }

  convertData(data: any) {
    data.dayLearning = data.days
    // delete data.days
    for (let i = 0; i < data.dayLearning.length; i++) {
      data.dayLearning[i].children = data.dayLearning[i].lessions
      // delete data.dayLearning[i].lessions
      for (let j = 0; j < data.dayLearning[i].children.length; j++) {
        data.dayLearning[i].children[j].children = data.dayLearning[i].children[j].questions
        // delete data.dayLearning[i].children[0].questions
      } 
    }
  }

  clickDay(data: any) {
    this.indexCurrDay = data.index
    this.currPath = data.path
    this.ObjectView = this.Course.dayLearning[this.indexCurrDay]
  }
  clickLession(data: any) {
    this.indexLession = data.index
    this.currPath = data.path
    this.indexCurrDay = data.path[0]
    this.ObjectView = this.Course.dayLearning[this.indexCurrDay].children[this.indexLession]
  }
}
