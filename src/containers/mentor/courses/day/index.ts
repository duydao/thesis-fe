import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { sfrHttpClient } from 'src/api';
import { handleDataIndex } from 'src/helpers/handle-data-index'

@Component({
  template: require('./view.html'),
  name: 'course-day',
  props: {
    Course: {
      type: Object
    },
    DayLearning: {
      type: Object
    },
    DataX: {
      type: Object
    }
  },
})

export class CourseDay extends Vue {
  async submit() {
    let check = true
    for(let i = 0; i < this.$props.DayLearning.challenge.subIndex.length; i++) {
      this.$props.DayLearning.challenge.subIndex[i].score = Number(this.$props.DayLearning.challenge.subIndex[i].score)
      if (!this.$props.DayLearning.challenge.subIndex[i].score) {
        check = false
      }
    }
    if (check) {
      this.$props.DayLearning.challenge.isComplete = true
    }
    handleDataIndex(this.$props.DataX)
    
    sfrHttpClient.course.submitAnswer(this.$props.DataX)
      .then(() => {
        this.$notification['success']({
          message: 'Success',
          description: 'Submit successful',
          duration: 2,
        });
      })
      .catch(e => {
        this.$message.error(e?.message || 'Error')
      })
  }
}