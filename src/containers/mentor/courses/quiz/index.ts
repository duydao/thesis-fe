import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { sfrHttpClient } from 'src/api';
import { handleDataIndex } from 'src/helpers/handle-data-index'

@Component({
  template: require('./view.html'),
  name: 'course-quiz',
  components: { 
    
  },
  props: {
    Quiz: {
      type: Array
    },
    Lession: {
      type: Object
    },
    DataX: {
      type: Object
    }
  },
})

export class CourseQuiz extends Vue {
  public check = false

  mounted() {
    this.checkType()
  }

  checkType() {
    for(let i = 0; i < this.$props.Quiz.length; i++) {
      if (this.$props.Quiz[i].type != 2) {
        this.check = true
      }
      this.$props.Quiz[i].scoreValue = this.$props.Quiz[i].scoreValue ? this.$props.Quiz[i].scoreValue : null
    }
  }

  async submit() {
    let totalScore = 0
    for(let i = 0; i < this.$props.Quiz.length; i++) {
      if (this.$props.Quiz[i].type == 2) {
        this.$props.Quiz[i].scoreValue = Number(this.$props.Quiz[i].score)
      } else {
        if (Number(this.$props.Quiz[i].scoreValue) < Number(this.$props.Quiz[i].score)) {
          this.$props.Quiz[i].scoreValue = Number(this.$props.Quiz[i].scoreValue)
        } else {
          this.$props.Quiz[i].scoreValue = Number(this.$props.Quiz[i].score)
        }
      }
      totalScore += this.$props.Quiz[i].scoreValue
    }
    this.$props.Lession.totalScore = Number(totalScore)
    if (totalScore > 0) {
      this.$props.Lession.isComplete = true
    } else {
      this.$props.Lession.isComplete = false
    }
    handleDataIndex(this.$props.DataX)

    sfrHttpClient.course.submitAnswer(this.$props.DataX)
      .then(() => {
        this.$notification['success']({
          message: 'Success',
          description: 'Submit successful',
          duration: 2,
        });
      })
      .catch(e => {
        this.$message.error(e?.message || 'Error')
      })
  }
}