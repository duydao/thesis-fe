import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { CourseQuiz } from '../quiz';
import { sfrHttpClient } from 'src/api';

@Component({
  template: require('./view.html'),
  name: 'course-lession',
  components: { 
    'quiz': CourseQuiz,
  },
  props: {
    Course: {
      type: Object
    },
    Lession: {
      type: Object
    },
    DataX: {
      type: Object
    }
  },
})

export class CourseLession extends Vue {
  
}