import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { Report } from '../../../models/report'
import { FormModel } from 'ant-design-vue';
import moment from 'moment';
import { sfrHttpClient } from './../../../api';

Vue.use(FormModel)
@Component({
  template: require('./view.html'),
})


export class MentorReport extends Vue {
  public moment: any;

  public ListMentees = [
    {
      mentee: 'Tran Van A',
      reports: [
        {
          day_report: '5/21/2020',
          your_report: 'Report day 21/5 something here Report something here Report something here Report something here Report something here Report something here',
          reply: [
            {
              time_reply: new Date().toLocaleString(),
              author: 'Minh Thien',
              reply: 'Mentor reply something here reply something here reply something here reply something here reply something here reply something here',
            },
            {
              time_reply: new Date().toLocaleString(),
              author: 'Minh Thien',
              reply: 'Mentor reply something here reply something here reply something here reply something here reply something here reply something here',
            }
          ],
        },
        {
          day_report: '5/20/2020',
          your_report: 'Report day 20/5 something here Report something here Report something here Report something here Report something here Report something here',
          reply: [
            {
              time_reply: new Date().toLocaleString(),
              author: 'Minh Thien',
              reply: 'Mentor reply something here reply something here reply something here reply something here reply something here reply something here',
            },
          ],
        },
        {
          day_report: '5/19/2020',
          your_report: 'Report day 19/5 something here Report something here Report something here Report something here Report something here Report something here',
          reply: [
            {
              time_reply: new Date().toLocaleString(),
              author: 'Louis',
              reply: 'Mentor reply something here reply something here reply something here reply something here reply something here reply something here',
            },
            {
              time_reply: new Date().toLocaleString(),
              author: 'Louis',
              reply: 'Mentor reply something here reply something here reply something here reply something here reply something here reply something here',
            },
            {
              time_reply: new Date().toLocaleString(),
              author: 'Louis',
              reply: 'Mentor reply something here reply something here reply something here reply something here reply something here reply something here',
            },
          ],
        },
      ]
    },
    {
      mentee: 'Tran Van B',
      reports: [
        {
          day_report: '5/25/2020',
          your_report: 'Report day 25/5 something here Report something here Report something here Report something here Report something here Report something here',
          reply: [],
        },
        {
          day_report: '5/24/2020',
          your_report: 'Report day 24/5 something here Report something here Report something here Report something here Report something here Report something here',
          reply: [
            {
              time_reply: new Date().toLocaleString(),
              author: 'Minh Thien',
              reply: 'Mentor reply something here reply something here reply something here reply something here reply something here reply something here',
            },
          ],
        },
        {
          day_report: '5/22/2020',
          your_report: 'Report day 22/5 something here Report something here Report something here Report something here Report something here Report something here',
          reply: [
            {
              time_reply: new Date().toLocaleString(),
              author: 'Louis',
              reply: 'Mentor reply something here reply something here reply something here reply something here reply something here reply something here',
            },
            {
              time_reply: new Date().toLocaleString(),
              author: 'Louis',
              reply: 'Mentor reply something here reply something here reply something here reply something here reply something here reply something here',
            },
            {
              time_reply: new Date().toLocaleString(),
              author: 'Louis',
              reply: 'Mentor reply something here reply something here reply something here reply something here reply something here reply something here',
            },
          ],
        },
      ]
    }
  ]
  
  private mentee: any = null
  private list_reports: any = null
  private report: any = null
  private formNewReply: any = {}

  isLoading = false;
  reportDetail: any = null;
  repReport = null;
  listInters= [];
  formReport = { 
    date:  moment(new Date()),
    mentorId: null
  }

  private time: any;
  mounted() {
    this.getListInter();
    this.timeoutData();
  }

  destroyed() {
    if(this.time) clearInterval(this.time)
  }

  clickMentee(data: any) {
    this.mentee = data.mentee
    this.list_reports = this.mentee.reports
  }

  clickDate(data: any) {
    this.report = data.report
  }

  handleNewReply() {
    if(!this.repReport || !this.reportDetail){
      this.$message.error("Form invalid");
      return;
    }
    this.isLoading = true;
    sfrHttpClient.reports.updateReportMentor({
        mentorId: this.formReport.mentorId,
        repReport: this.repReport
      }, this.reportDetail?.id)
      .then(() => {
        this.$message.success('Report success');
        this.getReport();
      })
      .catch(e => {
        this.$message.error(e?.message || 'Error')
      })
      .finally(() => {
        this.isLoading = false;
      })
  }

  disabledDate(current: any) {
    return current && current > moment().endOf('day');
  }

  async getReport(){
    this.repReport = null;
    this.reportDetail = await sfrHttpClient.reports.getReportMentor(this.formReport).catch(() => null)
  }

  private async getListInter(){
    this.listInters = await sfrHttpClient.users.getListIntern()
      .then((res) => {
        if(!!res.length){
          this.formReport.mentorId = res[0].id;
          this.getReport();
        }
        return res;
      })
      .catch(() => [])
  }

  private async timeoutData(){
    this.time = setInterval(async () => {
      if(this.formReport.mentorId){
        const reportDetail = await sfrHttpClient.reports.getReportMentor(this.formReport).catch(() => null)
        if(reportDetail?.repReport !== this.reportDetail?.repReport){
          this.reportDetail = reportDetail;
        }
      }
    }, 3000)
  }
}