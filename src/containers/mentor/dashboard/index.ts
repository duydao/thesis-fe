import Vue from 'vue';
import Component from 'vue-class-component';
import './style.scss';
import { sfrHttpClient } from './../../../api';

@Component({
  template: require('./view.html'),
})

export class MentorDashBoard extends Vue {
  public listCourse: any = []
  public listSubject: any = []
  public listReport: any = []

  public form = {
    subjectId: null,
    subjectName: null,
    coursesId: null,
    coursesName: null
  }

  mounted() {
    this.getDataDefault();
  }

  async changeCourse(item: any){
    if(item){
      this.form.subjectId = item.subjectId;
      this.form.coursesName = item.title;
      await this.getDataSubject();
      await this.getDateReport();
    }
  }

  redirectToPageStatic(){
    if(!this.form.coursesId) return;
    this.$router.push({ path: '/statistical', query: { ...this.form }})
  }

  private async getDataDefault(){
    this.listCourse = await sfrHttpClient.user.getListCourse().catch(() => [])
    if(this.listCourse?.length){
      this.form.coursesId = this.listCourse[0].id;
      this.form.coursesName = this.listCourse[0].title;
      this.form.subjectId = this.listCourse[0].subjectId;
      await this.getDataSubject();
      await this.getDateReport();
    }
  }

  private async getDataSubject(){
    this.listSubject = await sfrHttpClient.subjects.getDetailSubject(this.form.subjectId || '').then(r => {
      this.form.subjectName = r.name
      return [r]
    }).catch(() => [])
  }

  private async getDateReport(){
    this.listReport = await sfrHttpClient.reports.reportMentor(this.form).catch(() => [])
    window.scrollTo(0,0)
  }
}