declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

declare module 'ckeditor4-vue';
declare module 'vue-cropper';
