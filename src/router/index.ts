import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes';

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  scrollBehavior: (to, from) => {
    if (to.path === from.path) { return }
    return { x: 0, y: 0};
  },
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if(to.meta && to.meta.isPrivate){
    const user = JSON.parse(localStorage.getItem('@user') || '{}')
    if(!user.id){
      next({ name: 'Login', query: { redirect: to.fullPath }});
      return;
    }
    if(!to.meta.roleName.includes(user.roleName)){
      next({ name: 'dashboard'});
    }
  }
  next();
});


export default router
