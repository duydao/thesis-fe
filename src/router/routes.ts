import { RouteConfig } from 'vue-router';
import { 
  Layout, EditCourses, 
  NewCourse, Login,
  NewAccount, ForgotPassword, ResetPassword,
  ManageCourses, InternCourses,
  InternReport, InternDashBoard, 
  MentorCourses, MentorReport,
  MentorDashBoard, Assign,
  NewReport, ManageSubIndex,
  Statistical, StatisticalDetail,
  Dashboard
} from './../containers'


const routes: Array<RouteConfig> = [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: '/',
        name: "dashboard",
        component: Dashboard,
        meta: {
          isPrivate: true,
          roleName: ['Super Admin', 'Intern', 'Mentor']
        },
      },
      {
        path: '/home_inter',
        name: "dashboard",
        component: InternDashBoard,
        meta: {
          isPrivate: true,
          roleName: ['Intern']
        },
      },
      {
        path: '/home',
        name: "dashboard",
        component: MentorDashBoard,
        meta: {
          isPrivate: true,
          roleName: ['Mentor']
        },
      },
      {
        path: '/courses',
        component: MentorCourses,
        name: "courses",
        meta: {
          isPrivate: true,
          roleName: ['Super Admin', 'Mentor']
        },
      },
      {
        path: '/course',
        component: InternCourses,
        name: "course",
        meta: {
          isPrivate: true,
          roleName: ['Intern']
        },
      },
      {
        path: '/manage-courses',
        component: ManageCourses,
        name: "manage-courses",
        meta: {
          isPrivate: true,
          roleName: ['Super Admin', 'Mentor']
        },
      },
      {
        path: '/course/:id',
        component: EditCourses,
        name: "course-edit", // 
        meta: {
          isPrivate: true,
          roleName: ['Super Admin', 'Mentor']
        },
      },
      {
        path: '/new-course',
        component: NewCourse,
        name: "new-course",
        meta: {
          isPrivate: true,
          roleName: ['Super Admin', 'Mentor']
        },
      },
      {
        path: '/reports',
        component: InternReport,
        name: "reports",
        meta: {
          isPrivate: true,
          roleName: ['Intern']
        },
      },
      {
        path: '/list-reports',
        component: MentorReport,
        name: "list-reports",
        meta: {
          isPrivate: true,
          roleName: ['Mentor', 'Super Admin']
        },
      },
      {
        path: '/new-report',
        component: NewReport,
        name: "new-report",
        meta: {
          isPrivate: true,
          roleName: ['Intern']
        },
      },
      {
        path: '/new-account',
        component: NewAccount,
        name: "new-account",
        meta: {
          isPrivate: true,
          roleName: ['Super Admin']
        },
      },
      {
        path: '/assign',
        component: Assign,
        name: "assign",
        meta: {
          isPrivate: true,
          roleName: ['Super Admin']
        },
      },
      {
        path: '/manage-index',
        component: ManageSubIndex,
        name: "manage-index",
        meta: {
          isPrivate: true,
          roleName: ['Super Admin']
        },
      },
      {
        path: '/statistical',
        component: Statistical,
        name: "statistical",
        meta: {
          isPrivate: true,
          roleName: ['Super Admin', 'Mentor']
        },
      },
      {
        name: 'statistical-detail',
        path: '/statistical-detail',
        component: StatisticalDetail,
        meta: {
          isPrivate: true,
          roleName: ['Super Admin', 'Mentor']
        },
      },
      // {
      //   path: '/manage-intern',
      //   component: ListInterns,
      //   meta: {
      //     isPrivate: true
      //   },
      // },
      // {
      //   path: '/manage-mentor',
      //   component: ListMentors,
      //   meta: {
      //     isPrivate: true
      //   },
      // },
    ],
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/forgot-password',
    component: ForgotPassword,
  },
  {
    path: '/reset-password',
    component: ResetPassword,
  },
  {
    path: '*',
    redirect: '/'
  }
];

export default routes;