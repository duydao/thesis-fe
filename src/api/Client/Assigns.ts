import { ISimpleUser, IUser, IUserFilter } from 'models';
import { IHttpServiceClientOptions } from '../Core';

export interface IAssignsHttpService{
  createAssigns(form: any): Promise<any>;
}

export function AssignsHttpService(options: IHttpServiceClientOptions): IAssignsHttpService {
  const basePath = '/assigns';

  function createAssigns(form: any) {
    const _path = options.httpService.getUrl(`${basePath}`, options.mock);

    return options.httpService.post(_path, form);
  }

  return {
    createAssigns
  };
}
