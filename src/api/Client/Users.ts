import { ISimpleUser, IUser, IUserFilter } from 'models';
import { IHttpServiceClientOptions } from '../Core';
import { FormCreateAccount } from 'models/User'

export interface IUsersHttpService{
  createAccount(payload: FormCreateAccount): Promise<any>;
  updateUser(payload:any):Promise<any>;
  getListMentor(): Promise<any>;
  getListIntern(): Promise<any>;
  getListUser(roleId?: string): Promise<any>;
}

export function UsersHttpService(options: IHttpServiceClientOptions): IUsersHttpService {  
  const basePath = '/users';
  
  function createAccount (payload: FormCreateAccount) {
    const _path = options.httpService.getUrl(`${basePath}`, options.mock);

    return options.httpService.post(_path, payload);
  }

  function updateUser(params:any){
    const _path = options.httpService.getUrl(`${basePath}`+"/"+params.id, options.mock);
    return options.httpService.post(_path, params);
  }

  function getListMentor () {
    const _path = options.httpService.getUrl(`${basePath}/getListMentor`, options.mock);

    return options.httpService.get(_path);
  }

  function getListIntern () {
    const _path = options.httpService.getUrl(`${basePath}/getListIntern`, options.mock);

    return options.httpService.get(_path);
  }

  function getListUser (roleId?: string) {
    const _path = options.httpService.getUrl(`${basePath}${roleId ? '?roleId='+ roleId : ''}`, options.mock);

    return options.httpService.get(_path);
  }

  return {
    createAccount,
    getListMentor,
    getListIntern,
    getListUser,
    updateUser
  };
}
