import { ISimpleUser, IUser, IUserFilter } from 'models';
import { IHttpServiceClientOptions } from '../Core';

export interface ISubjectsHttpService{
  listSubject(): Promise<any>;
  createSubject(payload: {name: string}): Promise<any>;
  editSubject(payload: {name: string}, id: string): Promise<any>;
  getCoursesBySubject(id: string): Promise<any>;
  deleteSubject(form: any): Promise<any>;
  getDetailSubject(id?: string): Promise<any>;
}

export function SubjectsHttpService(options: IHttpServiceClientOptions): ISubjectsHttpService {  
  const basePath = '/subjects';
  
  function listSubject () {
    const _path = options.httpService.getUrl(`${basePath}`, options.mock);

    return options.httpService.get(_path);
  }

  function createSubject (payload: {name: string}) {
    const _path = options.httpService.getUrl(`${basePath}`, options.mock);

    return options.httpService.post(_path, payload) ;
  }

  function editSubject (payload: {name: string}, id: string) {
    const _path = options.httpService.getUrl(`${basePath}/${id}`, options.mock);

    return options.httpService.put(_path, payload) ;
  }

  function getCoursesBySubject (id: string) {
    const _path = options.httpService.getUrl(`${basePath}/${id}/courses`, options.mock);
    return options.httpService.get(_path);
  }

  function deleteSubject(SubjectId: any) {
    const _path = options.httpService.getUrl(`${basePath}/${SubjectId}`, options.mock);
    return options.httpService.del(_path);
  }

  function getDetailSubject(id?: string) {
    const _path = options.httpService.getUrl(`${basePath}/${id}`, options.mock);
    return options.httpService.get(_path);
  }

  return {
    listSubject,
    createSubject,
    getCoursesBySubject,
    deleteSubject,
    getDetailSubject,
    editSubject
  };
}
