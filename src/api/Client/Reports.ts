import { IHttpServiceClientOptions } from '../Core';

export interface IReportsHttpService{
  createReport(payload: any): Promise<any>;
  getReportInter(payload: any): Promise<any>;
  getReportMentor(payload: any): Promise<any>;
  reportMentor(payload: any): Promise<any>;
  updateReportMentor(payload: any, id: string): Promise<any>;
  getStatistical(params: any): Promise<any>;
}

export function ReportsHttpService(options: IHttpServiceClientOptions): IReportsHttpService {  
  const basePath = '/reports';
  
  function createReport (payload: any) {
    const _path = options.httpService.getUrl(`${basePath}`, options.mock);

    return options.httpService.post(_path, payload);
  }

  function getReportInter (payload: any) {
    const _path = options.httpService.getUrl(`${basePath}/intern`, options.mock);

    return options.httpService.post(_path, payload);
  }
  
  function getReportMentor (payload: any) {
    const _path = options.httpService.getUrl(`${basePath}/mentor`, options.mock);

    return options.httpService.post(_path, payload);
  }

  function reportMentor (payload: any) {
    const _path = options.httpService.getUrl(`${basePath}/reportMentor`, options.mock);

    return options.httpService.post(_path, payload);
  }

  function updateReportMentor (payload: any, id: string) {
    const _path = options.httpService.getUrl(`${basePath}/mentor/${id}`, options.mock);

    return options.httpService.post(_path, payload);
  }

  function getStatistical(params: any) {
    const _path = options.httpService.getUrl(`${basePath}/getStatictical`, options.mock);
    
    return options.httpService.post(_path, params);
  }

  return {
    createReport,
    getReportInter,
    getReportMentor,
    updateReportMentor,
    reportMentor,
    getStatistical
  };
}
