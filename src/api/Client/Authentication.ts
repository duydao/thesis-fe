import { ISimpleUser, IUser, IUserFilter } from 'models';
import { IHttpServiceClientOptions } from '../Core';
import { loginParams } from 'models/Authentication'

export interface IAuthenHttpService{
  authenticate(payload: loginParams): Promise<any>;
  forgotPassword(payload: {email: string}): Promise<any>;
  changePassword(payload: {password: string, token: string}): Promise<any>;
}

export function AuthenHttpService(options: IHttpServiceClientOptions): IAuthenHttpService {  
  const basePath = '/users';
  
  function authenticate (payload: loginParams) {
    const _path = options.httpService.getUrl(`${basePath}/login`, options.mock);

    return options.httpService.post(_path, payload);
  }

  function forgotPassword (payload: {email: string}) {
    const _path = options.httpService.getUrl(`${basePath}/forgot-password`, options.mock);

    return options.httpService.post(_path, payload);
  }

  function changePassword (payload: {password: string, token: string}) {
    const _path = options.httpService.getUrl(`${basePath}/change-password`, options.mock);

    return options.httpService.post(_path, payload);
  }

  return {
    authenticate,
    forgotPassword,
    changePassword
  };
}
