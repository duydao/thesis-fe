import { IHttpServiceClientOptions } from '../Core';

export interface ISubIndexHttpService{
  getList(): Promise<any>;
  createItem(form: any): Promise<any>;
  updateItem(form: any, id: string): Promise<any>;
  deleteItem(id: string): Promise<any>;
}

export function SubIndexHttpService(options: IHttpServiceClientOptions): ISubIndexHttpService {
  const basePath = '/subIndex';

  function getList() {
    const _path = options.httpService.getUrl(`${basePath}`, options.mock);

    return options.httpService.get(_path);
  }

  function createItem(form: any) {
    const _path = options.httpService.getUrl(`${basePath}`, options.mock);

    return options.httpService.post(_path, form);
  }

  function updateItem(form: any, id: string) {
    const _path = options.httpService.getUrl(`${basePath}/${id}`, options.mock);
    return options.httpService.put(_path, form);
  }

  function deleteItem(id: string) {
    const _path = options.httpService.getUrl(`${basePath}/${id}`, options.mock);
    return options.httpService.del(_path);
  }

  return {
    getList,
    createItem,
    updateItem,
    deleteItem
  };
}
