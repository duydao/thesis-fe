import { HttpService, IHttpServiceOptions } from '../Core';
import { IUserHttpService, UserHttpService } from './User';
import { IAuthenHttpService, AuthenHttpService } from './Authentication';     
import { ICourseHttpService, CourseHttpService } from './Course';
import { RolesHttpService, IRolesHttpService } from './Roles'
import { UsersHttpService, IUsersHttpService } from './Users';
import { AttachmentHttpService, IAAttachmentHttpService } from './Attachment';
import { SubjectsHttpService, ISubjectsHttpService } from './Subjects'
import { AssignsHttpService, IAssignsHttpService } from './Assigns'
import { ReportsHttpService, IReportsHttpService } from './Reports';
import { SubIndexHttpService, ISubIndexHttpService } from './SubIndex';
export interface ISFRHttpClient {
  user: IUserHttpService;
  auth: IAuthenHttpService;
  course: ICourseHttpService;
  roles: IRolesHttpService;
  users: IUsersHttpService,
  attachment: IAAttachmentHttpService,
  subjects: ISubjectsHttpService,
  assigns: IAssignsHttpService,
  reports: IReportsHttpService,
  subIndex: ISubIndexHttpService,
}

export function SFRHttpClient(options: IHttpServiceOptions): ISFRHttpClient {
  
  const httpService = HttpService(options);
  return {
    user: UserHttpService({ httpService }),
    auth: AuthenHttpService({ httpService }),
    course: CourseHttpService({ httpService }),
    roles: RolesHttpService({  httpService }),
    users: UsersHttpService({  httpService }),
    attachment: AttachmentHttpService( { httpService }),
    subjects: SubjectsHttpService( { httpService }),
    assigns: AssignsHttpService( { httpService }),
    reports: ReportsHttpService( { httpService }),
    subIndex: SubIndexHttpService( { httpService }),
  };
}
