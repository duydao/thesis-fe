import { ISimpleUser, IUser, IUserFilter } from 'models';
import { IHttpServiceClientOptions } from '../Core';
import { loginParams } from 'models/Authentication'

export interface IAAttachmentHttpService{
  upload(payload: FormData): Promise<any>;
}

export function AttachmentHttpService(options: IHttpServiceClientOptions): IAAttachmentHttpService {  
  const basePath = '/attachment';
  
  function upload(payload: FormData) {
    const _path = options.httpService.getUrl(`${basePath}/upload`, options.mock);

    return options.httpService.post(_path, payload);
  }

  return {
    upload
  };
}
