import { ISimpleUser, IUser, IUserFilter } from 'models';
import { IHttpServiceClientOptions } from '../Core';

export interface IRolesHttpService{
  listRoles(): Promise<any>;
}

export function RolesHttpService(options: IHttpServiceClientOptions): IRolesHttpService {  
  const basePath = '/roles';
  
  function listRoles () {
    const _path = options.httpService.getUrl(`${basePath}`, options.mock);

    return options.httpService.get(_path);
  }

  return {
    listRoles
  };
}
