import { ISimpleUser, IUser, IUserFilter } from 'models';
import { IHttpServiceClientOptions } from '../Core';

export interface IUserHttpService{
  getUsersList(filter: IUserFilter): Promise<ISimpleUser>;
  getCurrentUser(): Promise<ISimpleUser>;
  getListCourse(): Promise<ISimpleUser>;
  getListIntern(): Promise<ISimpleUser>;
  getReportDashboard(): Promise<any>;
}

export function UserHttpService(options: IHttpServiceClientOptions): IUserHttpService {
  const basePath = '/me';

  function getUsersList(filter: IUserFilter = {
    keyword: ''
  }) {
    const _path = options.httpService.getUrl(`${basePath}`, options.mock);

    return options.httpService.get(_path, {
      params: filter
    });
  }

  function getReportDashboard() {
    const _path = options.httpService.getUrl('/reportInter', options.mock);
    return options.httpService.get(_path)
  }

  function getListIntern() {
    const _path = options.httpService.getUrl('/users/getListIntern', options.mock);
    return options.httpService.get(_path)
  }

  function getCurrentUser() {
    const _path = options.httpService.getUrl(`/users${basePath}`, options.mock);
    return options.httpService.get(_path);
  }

  function getListCourse() {
    const _path = options.httpService.getUrl('/users/getLisCourses', options.mock);
    return options.httpService.get(_path)
  }

  return {
    getUsersList,
    getCurrentUser,
    getListCourse,
    getListIntern,
    getReportDashboard
  };
}
