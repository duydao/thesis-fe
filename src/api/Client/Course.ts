import { ISimpleUser, IUser, IUserFilter } from 'models';
import { IHttpServiceClientOptions } from '../Core';

export interface ICourseHttpService{
  getCoursesList(): Promise<any>;
  getCoursesListOfSubject(SubjectId: any): Promise<any>;
  createCourses(form: any): Promise<any>;
  deleteCourse(form: any): Promise<any>;
  getCourse(form: any): Promise<any>;
  getOneCourseAssign(form: any): Promise<any>;
  submitAnswer(form: any): Promise<any>;
  UpdateCourse(form: any, courseId: any): Promise<any>;
}

export function CourseHttpService(options: IHttpServiceClientOptions): ICourseHttpService {
  const basePath = '/courses';

  function getCoursesList() {
    const _path = options.httpService.getUrl(`${basePath}`, options.mock);
    return options.httpService.get(_path);
  }

  function getCourse(idCourse: any) {
    const _path = options.httpService.getUrl(`${basePath}/${idCourse}`, options.mock);
    return options.httpService.get(_path);
  }

  function UpdateCourse(form: any, courseId: any) {
    const _path = options.httpService.getUrl(`${basePath}/${courseId}`, options.mock);
    return options.httpService.post(_path, form);
  }

  function getCoursesListOfSubject(SubjectId: any) {
    const pathListCourseOfSubject = `/subjects/${SubjectId}` + basePath
    const _path = options.httpService.getUrl(`${pathListCourseOfSubject}`, options.mock);
    return options.httpService.get(_path);
  }

  function createCourses(form: any) {
    const _path = options.httpService.getUrl(`${basePath}`, options.mock);
    return options.httpService.post(_path, form);
  }

  function deleteCourse(CourseId: any) {
    const _path = options.httpService.getUrl(`${basePath}/${CourseId}`, options.mock);
    return options.httpService.del(_path);
  }

  function getOneCourseAssign(data: any) {
    const _path = options.httpService.getUrl(`/assigns/getOne`, options.mock);
    return options.httpService.post(_path, data);
  }

  function submitAnswer(data: any) {
    const _path = options.httpService.getUrl(`/assigns/${data.id}`, options.mock);
    return options.httpService.post(_path, data);
  }

  return {
    getCoursesList,
    getCoursesListOfSubject,
    createCourses,
    deleteCourse,
    getCourse,
    getOneCourseAssign,
    submitAnswer,
    UpdateCourse,
  };
}
