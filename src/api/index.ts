import { SFRHttpClient } from './Client';

export const sfrHttpClient = SFRHttpClient({
  baseURL: process.env.VUE_APP_BASE_URL
});
