export function handleDataIndex(data: any) {
  calcProgress(data)
  calcGPA(data)
  calcKnowledge(data)
  calcSubIndex(data)
  calcRecommended(data)
}

export function calcProgress(data: any) {
  const coursesData = data.coursesData
  data.progress = 0
  data.daysComplete = 0
  const daysLength = coursesData.dayLearning.length
  for (let i = 0; i < daysLength; i++) {
    if (coursesData.dayLearning[i].isComplete) {
      data.daysComplete += 1
    }
    else {
      let length = daysLength
      let count = 0
      if (coursesData.dayLearning[i].challenge.status) {
        length += 1
        if (coursesData.dayLearning[i].challenge.isComplete) {
          count += 1
        } else {
          coursesData.dayLearning[i].challenge.isComplete = false
        }
      }
      for (let j = 0; j < coursesData.dayLearning[i].children.length; j++) {
        if (coursesData.dayLearning[i].children[j].isComplete) {
          count += 1
        } else {
          coursesData.dayLearning[i].children[j].isComplete = false
        }
      }
      if (count >= (3 * length / 4)) {
        data.daysComplete += 1
        coursesData.dayLearning[i].isComplete = true
      } else {
        coursesData.dayLearning[i].isComplete = false
      }
    }
  }
  coursesData.progress = (data.daysComplete < daysLength) ? Number((data.daysComplete / daysLength) * 100) : 100
  if (coursesData.progress >= 100) {
    coursesData.isCompleted = true
  }
}

function calcGPA(data: any) {
  const coursesData = data.coursesData
  let totalScoreDays = 0
  for (let i = 0; i < coursesData.dayLearning.length; i++) {
    if (coursesData.dayLearning[i].isComplete) {
      let totalScoreLessions = 0
      const lessions = coursesData.dayLearning[i].children
      for (let j = 0; j < lessions.length; j++) {
        if (lessions[j].isComplete) {
          totalScoreLessions += Number(coursesData.dayLearning[i].children[j].totalScore)
        }
      }
      coursesData.dayLearning[i].score = Number(totalScoreLessions / lessions.length)
      totalScoreDays += Number(coursesData.dayLearning[i].score)
    }
  }
  coursesData.gpa = totalScoreDays / coursesData.dayLearning.length
}

function calcKnowledge(data: any) {
  const coursesData = data.coursesData
  let count = 0
  let totalScore = 0
  for (let i = 0; i < coursesData.dayLearning.length; i++) {
    const lessions = coursesData.dayLearning[i].children
    for (let j = 0; j < lessions.length; j++) {
      const questions = lessions[j].children
      if (lessions[j].isComplete) {
        for (let k = 0; k < questions.length; k++) {
          if (questions[k].type == 1) {
            count += 1
            totalScore += questions[k].scoreValue
          }
        }
      }
    }
  }
  coursesData.knowledge = (count > 0 && totalScore > 0) ? Number(totalScore / count) : 0
}

function calcSubIndex(data: any) {
  const coursesData = data.coursesData
  const daysLength = coursesData.dayLearning.length
  const total: any = []
  let count = 0

  for (let i = 0; i < daysLength; i++) {
    if (coursesData.dayLearning[i].challenge.status) {
      if (coursesData.dayLearning[i].challenge.isComplete) {
        for (let j = 0; j < coursesData.dayLearning[i].challenge.subIndex.length; j++) {
          if (coursesData.dayLearning[i].challenge.subIndex[j].score) {
            total[j] += Number(coursesData.dayLearning[i].challenge.subIndex[j].score)
          }
        }
        count += 1
      }
    }
  }
  for (let i = 0; i < coursesData.subIndexTotal.length; i++) {
    coursesData.subIndexTotal[i].score = (total[i] / count)
  }
}

function calcRecommended(data: any) {
  const coursesData = data.coursesData
  let subIndex = 0
  for (let i = 0; i < coursesData.subIndexTotal.length; i++) {
    subIndex += Number(coursesData.subIndexTotal[i].score) * Number(coursesData.subIndexTotal[i].rate / 100)
  }
  coursesData.recommended = Number((0.25 * coursesData.gpa) + (0.25 * coursesData.knowledge) + (0.5 * subIndex))
}
