var path = require('path')

module.exports = {
  runtimeCompiler: true,

  configureWebpack: {
    resolve: {
      alias: {
        src: path.resolve(__dirname, 'src')
      }
    },
    module: {
      rules: [
        {
          test: /.html$/,
          loader: "html-loader",
          exclude: /index.html/
        },
        {
          test: /.scss$/,
          loader: "sass-loader"
        }
      ]
    }
  }
}